from django.urls import path, register_converter
from django.contrib import admin
from django.views.generic import RedirectView, TemplateView
from comparing import views, views_ajax

from typing import Iterable

class TeamIDsConverter:
	regex = r'[0-9\,]*'
	def to_python(self, value: str) -> list[int]:
		return [int(x) for x in value.split(',')]
	def to_url(self, value: Iterable[int]) -> str:
		return ','.join(value)

register_converter(TeamIDsConverter, 'ints')

urlpatterns = [
	path('', views.main_page, name='main_page'),
	path('tournament/<int:tournament_id>/teams/', views.compare_teams_list_on_tournament, name='compare_teams_list_on_tournament'),
	path('tournament/<int:tournament_id>/teams/<ints:team_ids>/', views.compare_teams_list_on_tournament,
		name='compare_teams_list_on_tournament'),
	path('tournament2/<int:tournament_id>/teams/<ints:team_ids>/', views.compare_teams_list_on_tournament2,
		name='compare_teams_list_on_tournament2'),

	path('tournament/', views.all_teams_on_tournament, name='all_teams_on_tournament'),
	path('tournament/<int:tournament_id>/', views.all_teams_on_tournament, name='all_teams_on_tournament'),

	path('tournament/<int:tournament_id>/tours/', views.tournament_questions_table, name='tournament_questions_table'),
	path('tournament/<int:tournament_id>/tours/first/<int:n_first_tours>/', views.tournament_questions_table,
		name='tournament_questions_table'),

	path('ajax/tournaments/', views_ajax.tournament_list, name='ajax_tournament_list'),
	path('ajax/teams/', views_ajax.team_list, name='ajax_team_list'),

	path('tournament/<int:tournament_id>/update/', views.update_tournament, name='update_tournament'),
	path('tournament/<int:tournament_id>/history/', views.tournament_changes_history, name='tournament_changes_history'),
	path('tournament/<int:tournament_id>/questions/edit/', views.edit_tournament_questions, name='edit_tournament_questions'),
	path('tournament/<int:tournament_id>/corr/', views.corr_graphs, name='corr_graphs'),

	path('process_teams_form/', views.process_teams_form, name='process_teams_form'),
	path('compare_teams/<int:team1_id>,<int:team2_id>/', views.compare_two_teams, name='compare_two_teams'),

	path('team/<int:team_id>/', views.team_details, name='team_details'),
	path('team/<int:team_id>/full/<int:show_all>/', views.team_details, name='team_details'),

	path('team/<int:team_id>/history/', views.team_changes_history, name='team_changes_history'),

	path('venue/<int:venue_id>/', views.venue_details, name='venue_details'),

	path('ratings/', views.team_ratings, name='team_ratings'),

	path('status/', views.status, name='status'),
	path('clear_cache/', views.clear_cache, name='clear_cache'),
	path('update_siberia/', views.update_siberia, name='update_siberia'),

	path('photos_contest/', views.photos_contest, name='photos_contest'),
	path('photos_contest/photos/', TemplateView.as_view(template_name='comparing/photos_contest3.html', content_type='text/html')),

	path('vosen/', views.vosen, name='vosen'),
	path('vosen2/', views.vosen2, name='vosen2'),

	path('ban/', views.banalnosti, name='banalnosti'),

	path('stat_misc/', views.stat_misc, name='stat_misc'),

	path('admin/', admin.site.urls),

	path('robots.txt/', TemplateView.as_view(template_name='misc/robots.txt', content_type='text/plain')),

	path('small/', views.small),

	path('ny/', RedirectView.as_view(url='https://docs.google.com/spreadsheets/d/1kFjrQayGacLiXtRdTJblfwCkFnTQsBp0v-qcO74mbUo/edit#gid=2142567241'),
		name='ny_calendar'),
]
