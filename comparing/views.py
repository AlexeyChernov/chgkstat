from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import cache_page
from django.forms import modelformset_factory
from django.urls import reverse
from django.db.models.query import Prefetch
from django.utils.timezone import now
from django.contrib import messages
from django.db.models import Count
from django.conf import settings

import json
import datetime
from collections import OrderedDict, Counter
from operator import itemgetter
import os
import glob
import math

import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from comparing import models, forms, comparing_util, views_stat, rating_api
from typing import Iterable

MAX_TEAMS_TO_COMPARE = 400
MAX_TEAMS_TO_DRAW_GRAPHS = 40

# This prevents makemigrations on empty DB.
# Set to empty list to work around.
ALL_TEAMS_IN_BASE = {team.id: team for team in models.Team.objects.all()}

def score2symbol(score):
	if score == '1':
		return '<strong>+</strong>'
	if score == '0':
		return '-'
	return score

def score2style(score):
	if score == '1':
		return 'taken'
	if score == '?':
		return 'debated'
	return ''

def digit2symbol(score):
	if score == 1:
		return '<strong>+</strong>'
	if score == 9:
		return 'X'
	return '-'

def digit2style(score):
	if score == 1:
		return 'taken'
	if score == 9:
		return 'secondary'
	return ''

N_LAST_TOURNAMENTS = 50
def main_page(request):
	context = {}
	context['tournaments'] = models.Tournament.objects.filter(date_end__lte=datetime.date.today() - datetime.timedelta(days=3)).exclude(
		name__startswith="Онлайн: ").order_by('-date_end')[:N_LAST_TOURNAMENTS]
	return render(request, 'comparing/main_page.html', context)

def photos_contest(request):
	context = {}
	context['page_title'] = 'Конкурс на лучшие фотографии призёров очных турниров по спортивному «Что? Где? Когда?»'
	return render(request, 'comparing/photos_contest.html', context)

def banalnosti(request):
	context = {}
	context['page_title'] = 'Правила игры «Банальности»'
	return render(request, 'comparing/banalnosti.html', context)

def online_games(request):
	context = {}
	context['page_title'] = 'Онлайн-отыгрыши турниров по «Что? Где? Когда?»'
	return render(request, 'comparing/online_games.html', context)

def get_client_ip(request):
	x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
	if x_forwarded_for:
		ip = x_forwarded_for.split(',')[-1].strip()
	else:
		ip = request.META.get('REMOTE_ADDR')
	return ip

def create_file_name(tournament, teams, graph_type):
	return 't_t_graphs/{}_{}_{}.png'.format(graph_type, tournament.id, '-'.join(str(team.id) for team in teams))

# Return numpy array sorted by questions taken desc, then by team index
def get_sorted_arr(arr):
	res = arr[arr[:,0].argsort()[::-1]]
	return res[res[:,-1].argsort(kind='mergesort')[::-1]]

def draw_png(arr, teams, graph_type, n_questions_before_tour, fname, is_admin):
	n_questions = arr.shape[1] - 2
	width, height = plt.figure().get_size_inches()
	if n_questions > 36:
		width *= n_questions / 36
		if graph_type == 1:
			height *= math.sqrt(n_questions / 36)
		plt.figure(figsize=(width, height))

	arr_sorted = get_sorted_arr(arr)
	for i in range(arr_sorted.shape[0]):
		plt.plot(arr_sorted[i,1:], label=teams[int(arr_sorted[i,0])].name)

	plt.grid(axis='y')
	for i in n_questions_before_tour[1:]:
		plt.axvline(i, color='grey', linewidth=1)

	plt.xlim((0, n_questions))

	if True:
		lgd = plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
		# plt.savefig(settings.MEDIA_ROOT + '/' + fname)
		plt.savefig(settings.MEDIA_ROOT + '/' + fname, bbox_extra_artists=(lgd,), bbox_inches='tight')
	else:
		plt.legend()
		plt.savefig(settings.MEDIA_ROOT + '/' + fname)
	plt.close('all')

def simple_col_arr(arr):
	mins = np.min(arr, axis=0)
	maxes = np.max(arr, axis=0)
	res = np.array(arr.shape)
	for i in range(arr.shape[0]): # i = team number
		for j in range(arr.shape[1]): # j = question number, 0..n_questions
			if mins[j] == maxes[j]:
				res[i,j] = .5
			else:
				res[i,j] = (arr[i,j] - mins[j]) / (maxes[j] - mins[j])
	return res

def get_cool_arr(arr, step):
	mins = np.min(arr, axis=0)
	maxes = np.max(arr, axis=0)
	res = arr.astype(np.float16)
	n_teams = arr.shape[0]
	for j in range(1, arr.shape[1]): # j = question number, 1..n_questions+1
		res = get_sorted_arr(res)
		res = res[res[:,j].argsort(kind='mergesort')[::-1]]
		team_start = 0
		while team_start < n_teams:
			team_end = team_start
			while ((team_end + 1) < n_teams) and (res[team_end + 1,j] == res[team_start,j]):
				team_end += 1

			if maxes[j] == mins[j]:
				mean_value = .5
			else:
				mean_value = (res[team_start,j] - mins[j]) / (maxes[j] - mins[j])

			n_teams_with_equal_score = team_end - team_start
			if (maxes[j] > mins[j]) and (res[team_start,j] == mins[j]):
				start_value = 0
			elif (maxes[j] > mins[j]) and (res[team_start,j] == mins[j]):
				start_value = 1 - (n_teams_with_equal_score - 1) * step
			else:
				start_value = mean_value - ((n_teams_with_equal_score - 1) * step / 2)

			for i in range(team_start, team_end + 1):
				res[i,j] = start_value + (i - team_start) * step

			team_start = team_end + 1
	return get_sorted_arr(res)

# Creates two files with graphs. Returns their paths
# arr: first column - team numbers. # names.
# second column - zeros (No. of taken questions after 0 questions)
# next <n_questions> columns - number of taken questions after i questions 
def draw_graphs_return_context(tournament, teams, arr, n_questions_before_tour, is_admin):
	# fname1 = create_file_name(tournament, teams, 1)
	# draw_png(arr, teams, 1, n_questions_before_tour, fname1, is_admin)

	cool_arr = get_cool_arr(arr, step=0.01)
	# fname3 = create_file_name(tournament, teams, 3)
	# draw_png(cool_arr, teams, 3, n_questions_before_tour, fname3, is_admin)

	context = {}
	# context['graph_file_names'] = [fname1, fname3]

	arr_sorted = get_sorted_arr(arr)
	context['team_names'] = '\n'.join('data.addColumn("number", "{}");'.format(teams[arr_sorted[i,0]].name) for i in range(len(teams)))

	arr_data1 = ''
	for i in range(arr_sorted.shape[1] - 1):
		arr_data1 += '[' + str(i) + ', ' + ','.join(str(j) for j in arr_sorted[:, i + 1]) + '], '
	context['arr_data1'] = '[ ' + arr_data1[:-2] + ' ]'

	arr_data3 = ''
	for i in range(cool_arr.shape[1] - 1):
		arr_data3 += '[' + str(i) + ', ' + ','.join(str(j) for j in cool_arr[:, i + 1]) + '], '
	context['arr_data3'] = '[ ' + arr_data3[:-2] + ' ]'

	return context

def render_compare_teams_list_on_tournament(request, tournament, team_ids: Iterable[int], is_logged_in: bool=False):
	tour_lengths = tournament.get_tour_lengths()
	if not tour_lengths:
		messages.error(request, 'У этого турнира не указаны размеры туров. Будем надеяться, на сайте рейтинга это скоро поправят!')
		return redirect(tournament)

	context = {}
	teams = []
	nonexistent_team_ids = []
	teams_with_little_score_information = []
	for team_id in team_ids:
		team = models.Team.objects.filter(pk=team_id).first()
		if team:
			teams.append(team)
		else:
			nonexistent_team_ids.append(team_id)
	if len(teams) < 2:
		messages.error(request, 'Указанные вами команды не найдены, к сожалению. Попробуйте сравнить какие-нибудь ещё!')
		# return redirect(tournament)
	if len(nonexistent_team_ids) > 1:
		messages.error(request, 'Не найдены команды с номерами {}'.format(', '.join(nonexistent_team_ids)))
	elif nonexistent_team_ids:
		messages.error(request, 'Не найдена команда с номером {}'.format(nonexistent_team_ids[0]))

	context['tours'] = []
	n_questions_before_tour = [0]
	n_questions_in_prev_tours = 0
	context['teams'] = []
	context['question_by_n_teams'] = []
	last_team_that_took = []
	all_questions = {}

	for tour_number, tour_length in enumerate(tour_lengths):
		tour_dict = {}
		tour_questions = OrderedDict()
		n_questions_before_tour.append(n_questions_before_tour[tour_number] + tour_length)
		tour_dict['n_questions'] = tour_length
		for i in range(n_questions_in_prev_tours + 1, n_questions_in_prev_tours + tour_length + 1):
			question_dict = {}
			question_dict['question_text'] = tournament.question_text_set.filter(number=i).first()
			if tournament.n_teams_with_results:
				question_dict['question_data'] = tournament.question_data_set.filter(number=i).first()
			tour_questions[i] = question_dict
			all_questions[i - 1] = question_dict
		tour_dict['questions'] = tour_questions
		context['tours'].append(tour_dict)
		context['question_by_n_teams'].append([0] * tour_length)
		last_team_that_took.append([0] * tour_length)
		n_questions_in_prev_tours += tour_length

	results_json = models.comparing_util.url2json(tournament.get_all_teams_url())
	# tts = tournament.team_on_tournament_set.select_related('team')
	team_results_dict = {j['team']['id']: j for j in results_json}

	taken_masha = np.zeros((len(teams), tournament.questions_total + 2), dtype=np.uint16)

	team_number = -1
	for team in teams:
		if team.id not in team_results_dict:
			continue
		mask = team_results_dict[team.id].get('mask', [])
		mask = models.get_correct_mask(tournament, mask)
		if len(mask) < tournament.questions_total:
			teams_with_little_score_information.append(team)
			continue

		team_number += 1
		team_data = {}
		team_data['team'] = team
		team_data['on_tournament'] = tournament.team_on_tournament_set.filter(team=team).first()
		team_data['total_score'] = 0
		team_data['simplest_not_taken'] = None
		team_data['simplest_not_taken_number'] = None
		team_data['simplest_not_taken_nteams'] = 0
		team_data['hardest_taken'] = None
		team_data['hardest_taken_number'] = None
		team_data['hardest_taken_nteams'] = (tournament.n_teams_with_results + 1) if tournament.n_teams_with_results else None

		taken_masha[team_number, 0] = team_number # team.name

		team_data['tours'] = []
		for tour_number, tour_length in enumerate(tour_lengths):
			tour_data = {}
			tour_data['score_by_tour'] = 0
			tour_data['score'] = [''] * tour_length
			for question in range(tour_length):
				global_question_number = n_questions_before_tour[tour_number] + question # Starts from 0
				score = mask[global_question_number]
				tour_data['score'][question] = {'symbol': score2symbol(score), 'class': score2style(score)}
				if score == '1':
					tour_data['score_by_tour'] += 1
					team_data['total_score'] += 1
					context['question_by_n_teams'][tour_number][question] += 1
					last_team_that_took[tour_number][question] = team_number

					if tournament.n_teams_with_results:
						if team_data['hardest_taken_nteams'] > all_questions[global_question_number]['question_data'].n_teams_taken:
							team_data['hardest_taken'] = all_questions[global_question_number]
							team_data['hardest_taken_number'] = global_question_number + 1
							team_data['hardest_taken_nteams'] = all_questions[global_question_number]['question_data'].n_teams_taken
				# elif score == '?':
				# 	team_data['n_debated'] += 1
				elif score == 'X':
					if team_number == 0:
						context['tours'][tour_number]['questions'][global_question_number + 1]['is_dropped'] = True
				if (score == '0') and tournament.n_teams_with_results:
					if team_data['simplest_not_taken_nteams'] < all_questions[global_question_number]['question_data'].n_teams_taken:
						team_data['simplest_not_taken'] = all_questions[global_question_number]
						team_data['simplest_not_taken_number'] = global_question_number + 1
						team_data['simplest_not_taken_nteams'] = all_questions[global_question_number]['question_data'].n_teams_taken

				taken_masha[team_number, global_question_number + 2] = taken_masha[team_number, global_question_number + 1] \
					+ (1 if (score == '1') else 0)

			tour_data['score_from_start'] = team_data['total_score']
			team_data['tours'].append(tour_data)
		team_data['saved_by_me'] = 0
		context['teams'].append(team_data)

	if len(context['teams']) < 2:
		messages.error(request, 'Указанные вами команды не найдены, к сожалению. Попробуйте сравнить какие-нибудь ещё!')
		# return redirect(tournament)
	if teams_with_little_score_information:
		messages.error(request, 'Недостаточно данных об ответах на вопросы у команд с номерами {}'.format(
			', '.join(str(team.id) for team in teams_with_little_score_information)))

	for tour_number, tour_length in enumerate(tour_lengths):
		for question in range(tour_length):
			if context['question_by_n_teams'][tour_number][question] == 1:
				team_number = last_team_that_took[tour_number][question]
				context['teams'][team_number]['tours'][tour_number]['score'][question]['class'] = 'saved'
				context['teams'][team_number]['saved_by_me'] += 1
			elif context['question_by_n_teams'][tour_number][question] == 0:
				for team in context['teams']:
					team['tours'][tour_number]['score'][question]['class'] = 'secondary'

	if len(teams) <= MAX_TEAMS_TO_DRAW_GRAPHS:
		context.update(draw_graphs_return_context(tournament, teams, taken_masha, n_questions_before_tour, request.user.is_authenticated))

	context['page_title'] = 'Сравнение результатов команд на турнире {}'.format(tournament.name)
	context['tournament'] = tournament
	context['order_column'] = tournament.questions_total + 2 * tournament.tour_count
	context['time_generated'] = datetime.datetime.now()
	context['MAX_TEAMS_TO_COMPARE'] = MAX_TEAMS_TO_COMPARE
	context['is_logged_in'] = is_logged_in
	return render(request, 'comparing/teams.html', context)

@cache_page(60 * 60 * 24)
def render_cached_compare_teams_list_on_tournament(request, tournament, team_ids: Iterable[int], is_logged_in: bool=False):
	return render_compare_teams_list_on_tournament(request, tournament, team_ids, is_logged_in=is_logged_in)

def compare_teams_list_on_tournament(request, tournament_id, team_ids: Iterable[int]=[]):
	if 'compareTeams' in request.POST:
		team_ids_list = set()
		for key, val in request.POST.items():
			if key.startswith('team_'):
				team_ids_list.add(key[len('team_'):])
				if len(team_ids_list) >= MAX_TEAMS_TO_COMPARE:
					break
		return redirect('compare_teams_list_on_tournament', tournament_id=tournament_id, team_ids=team_ids_list)
	tournament = models.Tournament.objects.filter(pk=tournament_id).first()
	if (not tournament) or (not tournament.name):
		messages.error(request, 'Турнир с id {} не найден, к сожалению. Попробуйте какой-нибудь ещё!'.format(tournament_id))
		return redirect('main_page')
	team_ids_str = ','.join(str(id) for id in team_ids)[:240]
	models.Request_from_user.objects.create(tournament=tournament, team_list=team_ids_str, request_ip=get_client_ip(request))
	request_quantity, created = models.Request_quantity.objects.get_or_create(
		tournament=tournament,
		team_list=team_ids_str,
		defaults={'quantity': 1},
	)
	if not created:
		request_quantity.quantity += 1
		request_quantity.save()
	return render_cached_compare_teams_list_on_tournament(request, tournament, team_ids, is_logged_in = request.user.is_authenticated)

def compare_teams_list_on_tournament2(request, tournament_id, team_ids=''):
	tournament = models.Tournament.objects.filter(pk=tournament_id).first()
	if (not tournament) or (not tournament.name):
		messages.error(request, 'Турнир с id {} не найден, к сожалению. Попробуйте какой-нибудь ещё!'.format(tournament_id))
		return redirect('main_page')
	return render_compare_teams_list_on_tournament(request, tournament, team_ids)

@cache_page(60 * 60 * 2)
def render_cached_all_teams_on_tournament(request, tournament, is_logged_in=False):
	global ALL_TEAMS_IN_BASE
	if not tournament.is_archived:
		rating_api.update_tournament_and_results(tournament, comment='Обновление отдельного турнира', all_teams=ALL_TEAMS_IN_BASE)
	context = {}
	context['tournament'] = tournament
	context['page_title'] = 'Сравнение результатов команд на турнире {} ({})'.format(
		tournament.name, models.dates2str(tournament.date_start, tournament.date_end))

	tts = tournament.team_on_tournament_set.select_related('team__city__region__district', 'team__city__country', 'venue')
	context['tournament_has_questions_data'] = tournament.n_teams_with_results and (tournament.n_teams_with_results > 0)

	if context['tournament_has_questions_data']:
		geos = {}
		geos['venue'] = {}
		geos['city'] = {}
		geos['region'] = {}
		geos['district'] = {}
		geos['country'] = {}
		context['MAX_TEAMS_TO_COMPARE'] = MAX_TEAMS_TO_COMPARE
	elif tournament.date_end > datetime.date.today() - datetime.timedelta(days=14):
		context['no_results_recent_tournament'] = True
	else:
		context['no_results_old_tournament'] = True

	if context['tournament_has_questions_data']:
		context['data'] = []
		for tt in tts:
			chkbox_classes = []
			if tt.venue:
				context['show_venues'] = True

			region = district = country = None
			if tt.team.city:
				region = tt.team.city.region
				country = tt.team.city.country
				if region: # We have to check that region exists first to avoid exceptions
					district = region.district

			for field, name in [
					(tt.venue, 'venue'),
					(tt.team.city, 'city'),
					(region, 'region'),
					(district, 'district'),
					(country, 'country')]:
				if field:
					id = field.id
					chkbox_classes.append('{}{}'.format(name, id))
					if id in geos[name]:
						geos[name][id]['counter'] += 1
					else:
						geos[name][id] = {'counter': 1, 'name': field.name}
			context['data'].append((tt, ' '.join(chkbox_classes)))

		for name, dic in geos.items():
			context['dict_' + name] = OrderedDict()
			for key, val in sorted(dic.items(), key=lambda x: x[1]['name']):
				val['name'] += ' ({} команд{})'.format(val['counter'], comparing_util.plural_ending(val['counter'], 2))
				context['dict_' + name][key] = val

	context['time_generated'] = datetime.datetime.now()
	context['is_logged_in'] = is_logged_in
	return render(request, 'comparing/tournament_teams.html', context)

def all_teams_on_tournament(request, tournament_id=None, is_logged_in=False):
	if tournament_id is None:
		tournament_id = models.int_safe(request.GET.get('tournament_id', ''))
		if tournament_id:
			if 'team_list_submit' in request.GET:
				return redirect('all_teams_on_tournament', tournament_id=tournament_id)
			if 'questions_table_submit' in request.GET:
				return redirect('tournament_questions_table', tournament_id=tournament_id)
		messages.error(request, 'Турнир не найден, к сожалению. Попробуйте какой-нибудь ещё!')
		return redirect('main_page')
	tournament = models.Tournament.objects.filter(pk=tournament_id).first()
	if (not tournament) or (not tournament.name):
		messages.error(request, 'Турнир с id {} не найден, к сожалению. Попробуйте какой-нибудь ещё!'.format(tournament_id))
		return redirect('main_page')
	if len(models.team_geos) < 100:
		models.fill_team_geos()
	return render_cached_all_teams_on_tournament(request, tournament, is_logged_in=request.user.is_authenticated)

@cache_page(60 * 60 * 2)
def tournament_questions_table(request, tournament_id=None, n_first_tours=None):
	if tournament_id is None:
		tournament_id = models.int_safe(request.GET.get('tournament_id', ''))
		if tournament_id:
			return redirect('tournament_questions_table', tournament_id=tournament_id)
	if 'n_first_tours' in request.GET:
		n_first_tours = models.int_safe(request.GET['n_first_tours'])
		if n_first_tours:
			return redirect('tournament_questions_table', tournament_id=tournament_id, n_first_tours=n_first_tours)

	tournament = get_object_or_404(models.Tournament, pk=tournament_id)
	if not tournament.is_archived:
		rating_api.update_tournament_and_results(tournament, comment='Обновление отдельного турнира')
	context = {}
	context['page_title'] = 'Подробные результаты турнира {}'.format(tournament.name)
	context['tournament'] = tournament
	context['time_generated'] = datetime.datetime.now()

	tour_lengths, tour_lengths_ok, context['error'] = tournament.check_tour_lengths()
	if not tour_lengths_ok:
		return render(request, 'comparing/tournament_questions_table.html', context)

	n_tours = len(tour_lengths)
	if n_first_tours:
		n_first_tours = models.int_safe(n_first_tours)
		if n_first_tours > 0:
			n_tours = min(n_tours, n_first_tours)
			context['n_first_tours'] = n_first_tours
	n_questions_before_tour = [sum(tour_lengths[:i]) for i in range(n_tours + 1)]
	context['tours'] = [{} for i in range(n_tours)]
	context['n_tours_minus_one'] = len(tour_lengths) - 1

	questions_total = n_questions_before_tour[-1] if n_first_tours else tournament.questions_total

	questions_data = {q.number: q for q in tournament.question_data_set.order_by('number')}
	questions_text = {q.number: q for q in tournament.question_text_set.order_by('number')}
	if len(questions_data) < tournament.questions_total:
		context['error'] = 'Данных о взятиях на этом турнире у нас ещё нет.'
		return render(request, 'comparing/tournament_teams.html', context)

	for tour_number in range(n_tours):
		context['tours'][tour_number]['n_questions'] = tour_lengths[tour_number]
		tour_questions = OrderedDict()
		for question_number in range(n_questions_before_tour[tour_number], n_questions_before_tour[tour_number + 1]):
			question_dict = {}
			question_dict['question_text'] = questions_text.get(question_number + 1)
			question_dict['question_data'] = questions_data[question_number + 1]
			tour_questions[question_number + 1] = question_dict
		context['tours'][tour_number]['questions'] = tour_questions

	tts = tournament.team_on_tournament_set.select_related('team')
	context['teams'] = []
	last_team_that_took = [0] * questions_total
	context['n_teams_wo_results'] = 0
	for team_number, tt in enumerate(tts):
		pluses_array = tt.resultsAsArray()
		team_data = {}
		team_data['team'] = tt.team
		team_data['team_on_tournament'] = tt
		team_data['n_questions_taken'] = sum(pluses_array[:questions_total]) if n_first_tours else tt.n_questions_taken
		team_data['tours'] = [{} for i in range(n_tours)]
		if not pluses_array or (len(pluses_array) < questions_total):
			context['n_teams_wo_results'] += 1
			continue

		for tour_number in range(n_tours):
			tour_data = {}
			team_data['tours'][tour_number]['score_by_tour'] = sum(
				pluses_array[n_questions_before_tour[tour_number]:n_questions_before_tour[tour_number + 1]])
			team_data['tours'][tour_number]['score_from_start'] = sum(pluses_array[:n_questions_before_tour[tour_number + 1]])
			team_data['tours'][tour_number]['score'] = []
			for question in range(n_questions_before_tour[tour_number], n_questions_before_tour[tour_number + 1]):
				if questions_data[question + 1].is_dropped:
					question_dict = {'symbol': 'X', 'class': 'secondary'}
				else:
					score = pluses_array[question]
					question_dict = {'symbol': digit2symbol(score), 'class': digit2style(score)}
					if (score == 1) and (questions_data[question + 1].n_teams_taken == 1):
						question_dict['class'] = 'saved'
					elif questions_data[question + 1].n_teams_taken == 0:
						question_dict['class'] = 'secondary'
				team_data['tours'][tour_number]['score'].append(question_dict)
		context['teams'].append(team_data)

	context['hide_columns'] = (tts.count() > 50) or (questions_total > 36)
	return render(request, 'comparing/tournament_questions_table.html', context)

def getQuestionFormSet(tournament, data=None, files=None):
	QuestionFormSet = modelformset_factory(
		models.Question_text,
		form=forms.QuestionTextForm,
		can_delete=False,
		extra=tournament.questions_total,
		max_num=tournament.questions_total
	)
	initial = []
	existing_numbers = set(tournament.question_text_set.values_list('number', flat=True))
	initial = [{'number': i, 'tournament': tournament} for i in range(1, tournament.questions_total + 1) if i not in existing_numbers]
	return QuestionFormSet(
		data=data,
		files=files,
		queryset=tournament.question_text_set.order_by('number'),
		initial=initial,
	)

def edit_tournament_questions(request, tournament_id):
	tournament = get_object_or_404(models.Tournament, pk=tournament_id)
	if not tournament.name:
		messages.error(request, 'У турнира с id {} нет имени. Что-то не то'.format(tournament.id))
		return redirect('main_page')

	context = {}
	tour_lengths, tour_lengths_ok, error = tournament.check_tour_lengths()
	if not tour_lengths_ok:
		messages.error(request, error)
		return redirect('main_page')

	if not (request.user.is_authenticated or (tournament.id in (4984, 4985, 4986, 5217, )) ):
		messages.error(request, 'Редактировать вопросы турнира {} Вам, к сожалению, нельзя. Если хотите, чтобы было можно, напишите по адресу внизу!'.format(
			tournament.name))
		return redirect('main_page')

	if request.method == 'POST':
		formset = getQuestionFormSet(tournament, request.POST)
		if formset.is_valid():
			formset.save()
			messages.success(request, 'Вопросы успешно сохранены!')
			return redirect(tournament.get_edit_questions_url())
		else:
			messages.warning(request, 'Вопросы не сохранены. Пожалуйста, исправьте ошибки в форме')
			context['errors'] = unicode(formset.errors)
	else:
		formset = getQuestionFormSet(tournament)
	context['formset'] = formset
	context['tournament'] = tournament
	context['page_title'] = 'Редактирование вопросов турнира {} (id {})'.format(tournament.name, tournament.id)
	return render(request, 'editor/edit_tournament_questions.html', context)

@login_required
def team_changes_history(request, team_id):
	team = get_object_or_404(models.Team, pk=team_id)
	context = {}
	context['changes'] = models.Table_update.objects.filter(model_name=team.__class__.__name__, row_id=team.id).prefetch_related(
		Prefetch('field_update_set', queryset=models.Field_update.objects.order_by('field_name'))
	).order_by('-added_time')
	context['obj_link'] = '' # team.get_absolute_url()

	context['page_title'] = 'Команда {} (id {}): история изменений'.format(team, team.id)
	
	return render(request, 'comparing/changes_history.html', context)

@login_required
def tournament_changes_history(request, tournament_id):
	tournament = get_object_or_404(models.Tournament, pk=tournament_id)
	context = {}
	context['changes'] = models.Table_update.objects.filter(model_name=tournament.__class__.__name__, row_id=tournament.id).prefetch_related(
		Prefetch('field_update_set', queryset=models.Field_update.objects.order_by('field_name'))
	).order_by('-added_time')
	context['obj_link'] = tournament.get_absolute_url()

	context['page_title'] = 'Турнир {} (id {}): история изменений'.format(tournament, tournament.id)
	
	return render(request, 'comparing/changes_history.html', context)

N_LAST_UPDATES = 20
N_LAST_REQUESTS = 10
@login_required
def status(request):
	context = {}
	context['n_teams'] = models.get_stat_value('n_teams')
	context['n_tournaments'] = models.get_stat_value('n_tournaments')

	team_changes = models.Table_update.objects.filter(model_name=models.Team.__name__, action_type=models.ACTION_UPDATE).prefetch_related(
		Prefetch('field_update_set', queryset=models.Field_update.objects.order_by('field_name'))
	).order_by('-added_time')[:N_LAST_UPDATES]
	context['team_changes'] = [{'team': models.Team.objects.get(pk=tu.row_id), 'table_update': tu} for tu in team_changes]
	
	tournament_changes = models.Table_update.objects.filter(model_name=models.Tournament.__name__, action_type=models.ACTION_UPDATE).prefetch_related(
		Prefetch('field_update_set', queryset=models.Field_update.objects.order_by('field_name'))
	).order_by('-added_time')[:N_LAST_UPDATES]
	context['tournament_changes'] = [{'tournament': models.Tournament.objects.get(pk=tu.row_id), 'table_update': tu} for tu in tournament_changes]

	context['last_requests'] = models.Request_from_user.objects.order_by('-added_time')[:N_LAST_REQUESTS]
	context['most_popular_requests'] = models.Request_quantity.objects.order_by('-quantity')[:N_LAST_REQUESTS]

	today = now()
	this_week_start = (today - datetime.timedelta(days=7)).replace(hour=0, minute=0, second=0, microsecond=0)
	this_week_end = today.replace(hour=23, minute=59, second=59, microsecond=999999)
	prev_week_start = (today - datetime.timedelta(days=14)).replace(hour=0, minute=0, second=0, microsecond=0)
	prev_week_end = (today - datetime.timedelta(days=8)).replace(hour=23, minute=59, second=59, microsecond=999999)

	requests_this_week = models.Request_from_user.objects.filter(added_time__range=(this_week_start, this_week_end))
	this_week_tournaments = Counter(requests_this_week.values_list('tournament_id', flat=True))
	context['this_week_tournaments'] =[(models.Tournament.objects.get(pk=t_id), count) for t_id, count in this_week_tournaments.most_common(20)]

	context['n_requests_this_week'] = requests_this_week.count()
	context['n_requests_prev_week'] = models.Request_from_user.objects.filter(added_time__range=(prev_week_start, prev_week_end)).count()

	context['N_LAST_REQUESTS'] = N_LAST_REQUESTS
	context['page_title'] = 'Что у нас интересного'
	
	return render(request, 'comparing/status.html', context)

def _clear_cache():
	for f in glob.glob("/var/tmp/django_cache/*"):
		os.remove(f)
	with open(models.TOUCH_TO_RELOAD_PATH, 'a'):
		os.utime(models.TOUCH_TO_RELOAD_PATH, None)

def clear_cache(request):
	_clear_cache()
	messages.success(request, 'Кэш удалён')
	return redirect('main_page')

@login_required
def update_tournament(request, tournament_id):
	tournament = get_object_or_404(models.Tournament, pk=tournament_id)
	rating_api.update_tournament_and_results(tournament, comment='Обновление отдельного турнира')
	_clear_cache()
	messages.success(request, 'Данные о взятых вопросах на турнире успешно обновлены')
	return redirect(tournament)

def update_siberia(request):
	tournament = get_object_or_404(models.Tournament, pk=6227)
	rating_api.update_tournament_and_results(tournament, comment='Обновление отдельного турнира')
	_clear_cache()
	messages.success(request, 'Данные о командах и взятых вопросах на турнире «{}» успешно обновлены'.format(tournament.name))
	return redirect(tournament)

def team_ratings(request):
	context = {}
	context['page_title'] = 'Разные способы считать рейтинг активных команд в этом сезоне'
	context['MIN_TOURNAMENTS_FOR_RATINGS'] = models.MIN_TOURNAMENTS_FOR_RATINGS
	context['n_teams_with_1_game'] = views_stat.get_param_value('n_teams_with_1_game')
	context['n_teams_with_enough'] = views_stat.get_param_value('n_teams_with_enough')
	context['rating_release_date'] = views_stat.get_param_value('rating_release_date')

	context['top_teams'] = []
	for i in models.TOP_NUMBERS:
		value = models.int_safe(views_stat.get_param_value('n_teams_in_top_{}'.format(i)))
		if value:
			context['top_teams'].append((i, value, (value * 100) // i))

	ratings = {}
	for rating_type, _ in models.RATING_TYPES:
		ratings[rating_type] = {row['team_id']: row
			for row in models.Team_rating.objects.filter(rating_type=rating_type).values('team_id', 'value', 'place', 'n_games')}

	team_ids = sorted(ratings[0].keys())

	team_names = {row[0]: row[1]
			for row in models.Team.objects.filter(pk__in=team_ids).values_list('id', 'name')}

	context['teams'] = []
	for team_id in team_ids:
		data = {}
		data['team_id'] = team_id
		data['name'] = team_names[team_id]
		data['ratings'] = {}
		for rating_type, _ in models.RATING_TYPES:
			data['ratings'][rating_type] = ratings[rating_type][team_id]
		context['teams'].append(data)
	
	return render(request, 'comparing/team_ratings.html', context)

def try_get_team(team_id):
	return models.Team.objects.filter(pk=models.int_safe(team_id)).first() if team_id else None

def process_teams_form(request):
	team1_id = models.int_safe(request.GET.get('team1_id', ''))
	if not team1_id:
		messages.error(request, 'Команда с id {} не найдена, к сожалению. Попробуйте какую-нибудь ещё!'.format(team1_id))
		return redirect('main_page')
	if 'team_details' in request.GET:
		return redirect('team_details', team_id=team1_id)

	if 'compare_teams' not in request.GET:
		return redirect('main_page')

	team2_id = models.int_safe(request.GET.get('team2_id', ''))
	if not team2_id:
		messages.error(request, 'Команда с id {} не найдена, к сожалению. Попробуйте какую-нибудь ещё!'.format(team2_id))
		return redirect('main_page')
	return redirect('compare_two_teams', team1_id=team1_id, team2_id=team2_id)

@cache_page(60 * 60 * 24)
def compare_two_teams(request, team1_id, team2_id):
	teams = (get_object_or_404(models.Team, pk=team1_id), get_object_or_404(models.Team, pk=team2_id))
	context = {}
	context['teams'] = teams
	context['page_title'] = 'Сравнение результатов команд {} и {}'.format(teams[0].name, teams[1].name)

	# tournaments = [team.team_on_tournament_set.filter(tournament__is_in_rating=True).select_related('tournament').order_by(
	# 	'-tournament__date_end', '-tournament__date_start', 'tournament__name', 'pk') for team in teams]

	tournaments = [team.team_on_tournament_set.select_related('tournament').order_by(
		'-tournament__date_end', '-tournament__date_start', 'tournament__name', 'pk') for team in teams]

	tournament_ids = set(tournaments[0].values_list('tournament_id', flat=True)) & set(tournaments[1].values_list('tournament_id', flat=True))
	if tournament_ids:
		context['has_tournaments'] = True
		context['segments'] = OrderedDict()
		n_draws = 0
		n_draws_rating = 0
		n_questions_taken = [0, 0]
		n_questions_taken_rating = [0, 0]
		n_tournaments_won = [0, 0]
		n_tournaments_won_rating = [0, 0]
		n_tournaments = 0
		n_tournaments_rating = 0
		prev_first_date = datetime.date.today()

		for segment_index, segment_name, first_date in models.TIME_SEGMENTS:
			segment_data = {}
			new_tournaments = list(zip(*(list(t_t.filter(tournament_id__in=tournament_ids, tournament__date_end__gte=first_date,
				tournament__date_end__lt=prev_first_date)) for t_t in tournaments)))
			n_new_tournaments = len(new_tournaments)
			n_tournaments += n_new_tournaments
			segment_data['tournaments'] = []
			for t_ts in new_tournaments:
				winner_number = -1
				tournament = t_ts[0].tournament
				both_are_in_rating = tournament.is_in_rating and t_ts[0].is_included_in_rating and t_ts[1].is_included_in_rating
				if both_are_in_rating:
					n_tournaments_rating += 1

				for team_num in range(2):
					if t_ts[team_num].n_questions_taken is None:
						t_ts[team_num].n_questions_taken = 0
					n_questions_taken[team_num] += t_ts[team_num].n_questions_taken
					if both_are_in_rating:
						n_questions_taken_rating[team_num] += t_ts[team_num].n_questions_taken
				# print(t_ts[0].tournament_id, t_ts[0].n_questions_taken, t_ts[1].n_questions_taken, t_ts[0].bonus_b, t_ts[1].bonus_b)
				if (t_ts[0].n_questions_taken > t_ts[1].n_questions_taken) or \
						((t_ts[0].bonus_b is not None) and (t_ts[1].bonus_b is not None) and (t_ts[0].bonus_b > t_ts[1].bonus_b)):
					n_tournaments_won[0] += 1
					winner_number = 0
					if both_are_in_rating:
						n_tournaments_won_rating[0] += 1
				elif (t_ts[0].n_questions_taken < t_ts[1].n_questions_taken) or \
						((t_ts[0].bonus_b is not None) and (t_ts[1].bonus_b is not None) and (t_ts[0].bonus_b < t_ts[1].bonus_b)):
					n_tournaments_won[1] += 1
					winner_number = 1
					if both_are_in_rating:
						n_tournaments_won_rating[1] += 1
				else:
					n_draws += 1
					if both_are_in_rating:
						n_draws_rating += 1
				segment_data['tournaments'].append((t_ts, winner_number))

			segment_data['n_tournaments'] = n_tournaments
			segment_data['n_tournaments_rating'] = n_tournaments_rating
			segment_data['n_questions_taken'] = list(n_questions_taken)
			segment_data['n_questions_taken_rating'] = list(n_questions_taken_rating)
			segment_data['n_tournaments_won'] = list(n_tournaments_won)
			segment_data['n_tournaments_won_rating'] = list(n_tournaments_won_rating)
			segment_data['n_draws'] = n_draws
			segment_data['n_draws_rating'] = n_draws_rating
			context['segments'][segment_name] = segment_data
			prev_first_date = first_date

	return render(request, 'comparing/compare_two_teams.html', context)

N_PLAYERS_IN_TEAM = 20
@cache_page(60 * 60 * 24)
def team_details(request, team_id, show_all=False):
	team = get_object_or_404(models.Team, pk=team_id)
	context = {}
	context['team'] = team
	context['show_all'] = show_all
	context['page_title'] = f'Немного статистики про команду {team.name}'
	if team.city:
		context['page_title'] += f' ({team.city.name})'
	context['teams'] = [team]
	context['rating_date'] = models.date2str(models.Statistics.objects.filter(name='ratings_loaded').order_by('-date_added').first().date_added)
	context['played_like_me'] = team.team_close_to_mine_set.select_related('team2__city').order_by('abs_difference', 'deviation')
	context['MAX_TOGETHER_GAMES_TO_CHECK'] = models.MAX_TOGETHER_GAMES_TO_CHECK
	context['MIN_GAMES_IN_YEAR_TO_CHECK'] = models.MIN_GAMES_IN_YEAR_TO_CHECK

	context['time_segments'] = []
	for time_segment, segment_name, date_start in models.TIME_SEGMENTS:
		segment_data = {}
		segment_data['name'] = segment_name
		team_games = team.team_on_tournament_set.filter(tournament__is_in_rating=True, tournament__date_end__gte=date_start)
		segment_data['team_played'] = team_games.count()
		segment_data['team_played_offline'] = team_games.filter(tournament__t_type__in=models.IN_RATING_OFFLINE_TYPES).count()
		team_players = team.player_in_team_set.filter(time_segment=time_segment).select_related('player').order_by(
			'-n_games', 'player__surname', 'player__name', 'player__patronymic')
		n_team_players = team_players.count()
		if (not show_all) and (n_team_players > 20):
			team_players = team_players[:N_PLAYERS_IN_TEAM]
			n_team_players = 20
			segment_data['show_all_link'] = True
		half = max((n_team_players + 1) // 2, 10)
		team_players = list(enumerate(team_players))
		segment_data['team_players_part1'] = team_players[:half]
		segment_data['team_players_part2'] = team_players[half:]

		segment_data['most_often_teams'] = team.team_most_often_partner_set.filter(
			time_segment=time_segment, only_offline_games=False).select_related('team2').order_by('-n_games')

		segment_data['most_often_offline_teams'] = team.team_most_often_partner_set.filter(
			time_segment=time_segment, only_offline_games=True).select_related('team2').order_by('-n_games')

		context['time_segments'].append(segment_data)
	context['time_segments'][0]['desc'] = '(с {})'.format(models.date2str(models.SEASON_START))
	context['time_segments'][1]['desc'] = '(с {})'.format(models.date2str(datetime.datetime.strptime(
		views_stat.get_param_value('three_years_ago'), '%Y-%m-%d')))
	context['team_partners_filled'] = models.date2str(datetime.datetime.strptime(views_stat.get_param_value('team_partners_filled'), '%Y-%m-%d'))
	context['players_in_teams_filled'] = models.date2str(datetime.datetime.strptime(views_stat.get_param_value('players_in_teams'), '%Y-%m-%d'))
	return render(request, 'comparing/team_details.html', context)

def venue_details(request, venue_id):
	venue = get_object_or_404(models.Venue, pk=venue_id)
	tts = venue.team_on_tournament_set.select_related('tournament').annotate(n_players=Count('player_on_tournament'))
	games = {}
	for tt in tts:
		if tt.tournament not in games:
			games[tt.tournament] = {'n_teams': 0, 'n_players': 0, 'synch_request_ids': set([tt.synch_request_id])}
		games[tt.tournament]['n_teams'] += 1
		games[tt.tournament]['n_players'] += tt.n_players
		games[tt.tournament]['synch_request_ids'].add(tt.synch_request_id)
	context = {}
	context['page_title'] = 'Площадка {} ({}): все игры'.format(venue.name, venue.city.name if venue.city else 'город не указан')
	context['venue'] = venue
	context['games'] = sorted(games.items(), key=lambda x: x[0].date_end, reverse=True)

	if venue.id == 3081:
		players = Counter()
		year = 2019
		context['large_game_ids'] = set(tournament.id for tournament, data in games.items()
				if ((data['n_teams'] >= 4) or (tournament.id == 5752))
			) - set([5725])
		for pt in models.Player_on_tournament.objects.filter(
				team_on_tournament__venue_id=venue.id,
				team_on_tournament__tournament__date_start__year=year, 
				team_on_tournament__tournament_id__in=context['large_game_ids'],
				team_on_tournament__tournament__is_in_rating=True).exclude(
				team_on_tournament__synch_request_id__in=(76670, 67792, 66311, 66792, 67115, 69153, 69959)).select_related(
				'player'):
			players[pt.player] += 1
		context['players'] = players.most_common()
	return render(request, 'comparing/venue_details.html', context)

# @cache_page(60 * 60 * 24)
def stat_misc(request):
	context = {}
	context['page_title'] = 'Странная статистика'
	context['template_name'] = 'generated/stat_misc.html'
	return render(request, 'comparing/base_template.html', context)

def small(request):
	return render(request, 'comparing/small.html', {})

def corr_graphs(request, tournament_id):
	tournament = get_object_or_404(models.Tournament, pk=tournament_id)
	context = {}
	context['tournament'] = tournament
	context['corr_and_rating'] = '["Корреляция", "Плюс от турнира"],\n'
	context['corr_and_score'] = '["Корреляция", "Число взятых вопросов"],\n'
	for team, t_t, corr in views_stat.get_corrs(tournament):
		context['corr_and_rating'] += '[ {{v: {}, f: "{}, {:.2f}"}}, {}],\n'.format(corr, team.name, corr, t_t.diff_bonus)
		context['corr_and_score'] += '[ {{v: {}, f: "{}, {:.2f}"}}, {}],\n'.format(corr, team.name, corr, t_t.n_questions_taken)
	return render(request, 'comparing/corr_graphs.html', context)

def vosen(request):
	context = {}
	context['page_title'] = 'Благотворительный турнир «Восень патрыярха»'
	return render(request, 'comparing/vosen.html', context)

def vosen2(request):
	context = {}
	context['page_title'] = 'Благотворительный турнир «Адыходзячая восень»'
	return render(request, 'comparing/vosen2.html', context)
