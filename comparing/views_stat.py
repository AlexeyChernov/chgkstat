from django.template.loader import render_to_string
from django.db.models import Max, Sum, F
from django.shortcuts import render
from django.conf import settings

import datetime
import time
import io
import os
import numpy as np
from scipy import sparse as sp
from collections import Counter, defaultdict
from decimal import Decimal

from comparing import models, comparing_util

def fill_n_dropped_questions_for_tournaments():
	tournament_ids = set(models.Question_data.objects.all().values_list('tournament_id', flat=True).distinct())
	max_dropped = 0
	max_tournament = None
	for tournament in models.Tournament.objects.filter(pk__in=tournament_ids).order_by('pk'):
		tournament.questions_dropped = tournament.question_data_set.filter(is_dropped=True).count()
		tournament.save()
		if tournament.questions_dropped > max_dropped:
			max_dropped = tournament.questions_dropped
			max_tournament = tournament
	print(len(tournament_ids), max_dropped, max_tournament)

def fill_rounded_percents():
	counter = Counter()
	for q in models.Question_data.objects.all():
		q.percent_teams_taken_rounded = int(round(q.percent_teams_taken, -1))
		q.save()
		counter[q.percent_teams_taken_rounded] += 1
	for key, val in counter.items():
		print(key, val)

def get_total_number_of_pluses():
	good_tournament_ids = set(models.Tournament.objects.filter(
		t_type__in=models.IN_RATING_TYPES).values_list('id', flat=True))
	value = models.Questions_taken.objects.filter(team_on_tournament__tournament__in=good_tournament_ids).aggregate(
		total=Sum('n_questions'))['total']
	set_stat_value(name='n_pluses_and_minuses', value=value)
	print(value)

def fill_team_tournament_cities_once():
	for model in (models.Team, models.Tournament):
		n_objs_wo_city = 0
		n_objs_ok = 0
		n_objs_error = 0
		for obj in model.objects.order_by('pk'):
			if obj.city_name and (obj.city_name not in models.STRANGE_CITIES):
				n_objs_wo_city += 1
				continue
			city = models.City.objects.filter(name=obj.city_name).first()
			if not city:
				print('{} (id {}): city {} not found'.format(obj.name, obj.id, obj.city_name))
				n_objs_error += 1
				continue
			# obj.city = city
			# obj.save()
			n_objs_ok += 1
		print('Done with {}! OK: {}, wo city: {}, errors: {}'.format(model.__name__, n_objs_ok, n_objs_wo_city, n_objs_error))

def fill_tour_details_once():
	results = Counter()
	for tournament in models.Tournament.objects.order_by('id'):
		tournament_json = comparing_util.url2json(tournament.get_info_json_url())[0]
		details = tournament_json.get('tour_ques_per_tour')
		results[details] += 1
		if details:
			tournament.questions_per_tour_details = details
			tournament.save()
		if (tournament.id % 100) == 0:
			print(tournament.id)
	for key, val in results.items():
		print(key, val)
	print('Done!')

def show_tour_details():
	results = Counter()
	for tournament in models.Tournament.objects.order_by('id'):
		results[tournament.questions_per_tour_details] += 1
	for key, val in sorted(results.items(), key=lambda x:-x[1]):
		print(key, val)

def check_tournament_question_totals():
	no_info = 0
	only_total = 0
	error = 0
	correct = 0
	for tournament in models.Tournament.objects.order_by('id'):
		if tournament.questions_total:
			lengths = tournament.get_tour_lengths()
			if lengths:
				if sum(lengths) == tournament.questions_total:
					correct += 1
				else:
					error += 1
					print(tournament.id, tournament.questions_per_tour , tournament.questions_per_tour_details, tournament.tour_count,)
					print(tournament.questions_total, tournament.get_dates(), tournament.name)
			else:
				only_total += 1
		else:
			no_info += 1
	print('no_info:', no_info)
	print('only_total:', only_total)
	print('error:', error)
	print('correct:', correct)

def get_current_team_rating_json(team_id):
	url = models.get_team_cur_rating_json_url(team_id)
	try:
		return comparing_util.url2json(url)
	except:
		models.write_log("Team rating page broken: {}".format(url), debug=True)
		return None

def set_param_value(name, value): # Saving this way for last_update to update
	stat, _ = models.Parameter.objects.get_or_create(name=name)
	stat.value = value
	stat.save()

def get_param_value(name):
	stat = models.Parameter.objects.filter(name=name).first()
	return stat.value if stat else None

def get_weighted_average(pairs): # if pairs is a list of (value, weight)
	return sum(i * j for i, j in pairs) / sum(j for i, j in pairs)

models.MIN_TOURNAMENTS_FOR_RATINGS = 4
def fill_team_ratings():
	models.Team_rating.objects.all().delete()
	date_end_from = datetime.date(2018, 9, 2)
	good_participations = models.Team_on_tournament.objects.filter(
		tournament__date_end__gte=date_end_from, is_included_in_rating=True).values_list(
		'team_id', 'bonus_b', 'tournament__t_type', 'tournament_id')
	team_ratings = {}
	for team_id, bonus, t_type, tournament_id in good_participations:
		# if team_id == 67979:
		# 	print(team_id, bonus, t_type, tournament_id)
		if team_id not in team_ratings:
			team_ratings[team_id] = []
		weight = 1
		if t_type in models.IN_RATING_OFFLINE_TYPES:
			weight = 2
		elif t_type == 6:
			weight = 1.5
		team_ratings[team_id].append((bonus, weight))

	print('Total teams and playing a lot teams:', len(team_ratings), len(
		[team_id for team_id, ratings in team_ratings.items() if len(ratings) >= models.MIN_TOURNAMENTS_FOR_RATINGS]))
	set_param_value('n_teams_with_1_game', len(team_ratings))
	set_param_value('n_teams_with_enough', len(
		[team_id for team_id, ratings in team_ratings.items() if len(ratings) >= models.MIN_TOURNAMENTS_FOR_RATINGS]))
	
	n_top_teams = {i: 0 for i in models.TOP_NUMBERS}
	
	n_teams_broken = 0
	n_teams_worked = 0
	cur_rating_date = ''
	for team_id, ratings in team_ratings.items():
		# if team_id != 67979:
		# 	continue
		n_games = len(ratings)
		if n_games >= models.MIN_TOURNAMENTS_FOR_RATINGS:
			rating_json = get_current_team_rating_json(team_id)
			if rating_json:
				place = models.int_safe(rating_json['rating_position'])
				models.Team_rating.objects.create(team_id=team_id, rating_type=0, value=models.int_safe(rating_json['rating']), place=place)
				cur_rating_date = rating_json['date']
				for i in models.TOP_NUMBERS:
					if place <= i:
						n_top_teams[i] += 1
			else:
				n_teams_broken += 1

			ratings_sorted = sorted(ratings)
			# if team_id == 67979:
			# 	print(ratings_sorted)

			models.Team_rating.objects.create(team_id=team_id, rating_type=1, n_games=n_games,
				value=sum(i for i, _ in ratings_sorted[1:-1]) / (n_games - 2))

			cut_size = n_games // 8
			ratings_sum = sum(i for i, _ in ratings_sorted[cut_size:-cut_size]) if cut_size else sum(i for i, _ in ratings_sorted)
			models.Team_rating.objects.create(team_id=team_id, rating_type=2, n_games=n_games, value=ratings_sum / (n_games - 2 * cut_size))

			cut_size = ((n_games - 1) // 4) + 1 # ceil(n_games/4)
			models.Team_rating.objects.create(team_id=team_id, rating_type=3, n_games=n_games,
				value=get_weighted_average(ratings_sorted[cut_size:]))

			n_teams_worked += 1
			if (n_teams_worked % 1000) == 0:
				print(n_teams_worked)

	for rating_type, _ in models.RATING_TYPES[1:]:
		cur_place = 1
		last_place = 1
		last_rating = 99999
		for team_rating in models.Team_rating.objects.filter(rating_type=rating_type).order_by('-value'):
			if team_rating.value == last_rating:
				team_rating.place = last_place
			else:
				team_rating.place = cur_place
				last_place = cur_place
			team_rating.save()
			cur_place += 1
			last_rating = team_rating.value

	set_param_value('rating_release_date', cur_rating_date)
	for i in models.TOP_NUMBERS:
		set_param_value('n_teams_in_top_{}'.format(i), n_top_teams[i])

	# for team_rating in models.Team_rating.objects.filter(team_id=56459).order_by('-value'):
	# 	print(team_rating.rating_type, team_rating.value)

def fix_team_ratings():
	for rating_type, _ in models.RATING_TYPES[3:]:
		cur_place = 1
		last_place = 1
		last_rating = 99999
		for team_rating in models.Team_rating.objects.filter(rating_type=rating_type).order_by('-value'):
			if team_rating.value == last_rating:
				team_rating.place = last_place
			else:
				team_rating.place = cur_place
				last_place = cur_place
			team_rating.save()
			cur_place += 1
			last_rating = team_rating.value

def dump_team_tournament_matrix(debug=False):
	matrix, t_numbers, t_numbers_rev, team_numbers, team_numbers_rev = get_team_tournament_lists()
	np.savetxt("20190227_t_t.csv", matrix, fmt=str("%1d"), delimiter=",")
	with io.open("20190227_t_t_ids.csv", "a") as f:
		f.write(','.join(str(i) for i in t_numbers) + "\n")
		f.write(','.join(str(i) for i in team_numbers) + "\n")
	if debug:
		print(unicode(datetime.datetime.now()), 'Done!')

# Returns zero-filled (n_tournaments, n_teams) matrix and lists of numbers for cols and rows
def get_team_tournament_lists(only_offline, debug=False):
	tournaments = models.Tournament.objects.filter(is_in_rating=True)
	if only_offline:
		tournaments = tournaments.filter(t_type__in=models.IN_RATING_OFFLINE_TYPES)
	t_numbers = list(tournaments.order_by('pk').values_list('pk', flat=True))
	t_numbers_rev = {v: k for k, v in enumerate(t_numbers)}
	n_tournaments = len(t_numbers)

	all_team_numbers = set(models.Team_on_tournament.objects.filter(tournament_id__in=set(t_numbers)).values_list('team_id', flat=True))
	team_numbers = sorted(all_team_numbers)
	team_numbers_rev = {v: k for k, v in enumerate(team_numbers)}
	n_teams = len(team_numbers)

	matrix = np.zeros((n_tournaments, n_teams), dtype=int)
	return matrix, t_numbers, t_numbers_rev, team_numbers, team_numbers_rev

# Returns zero-filled (n_team_on_tournaments, n_players) matrix and lists of numbers for cols and rows
def get_tt_player_lists(only_offline, debug=False):
	tts = models.Team_on_tournament.objects.filter(tournament__is_in_rating=True)
	if only_offline:
		tts = tts.filter(tournament__t_type__in=models.IN_RATING_OFFLINE_TYPES)
	tt_numbers = list(tts.order_by('pk').values_list('pk', flat=True))
	tt_numbers_rev = {v: k for k, v in enumerate(tt_numbers)}

	player_numbers = sorted(models.Player_on_tournament.objects.filter(team_on_tournament_id__in=set(tt_numbers)).values_list(
		'player_id', flat=True))
	player_numbers_rev = {v: k for k, v in enumerate(player_numbers)}

	matrix = np.zeros((len(tt_numbers), len(player_numbers)), dtype=int)
	return matrix, tt_numbers, tt_numbers_rev, player_numbers, player_numbers_rev

# Adds 1's for given time segment to the matrix returned by get_team_tournament_lists
def update_t_t_matrix(matrix, t_numbers, t_numbers_rev, team_numbers_rev, time_segment, debug=False):
	max_thousand = (models.Team.objects.order_by('-pk').first().id - 1) // 1000 + 1
	t_max_thousand = (models.Tournament.objects.order_by('-pk').first().id - 1) // 1000 + 1
	for thousand in range(max_thousand):
		if debug:
			print(unicode(datetime.datetime.now()), 'Teams thousand:', thousand)
		id_from = thousand * 1000 + 1
		id_to = (thousand + 1) * 1000

		t_ts = models.Team_on_tournament.objects.filter(tournament_id__in=set(t_numbers), team_id__gte=id_from, team_id__lte=id_to)
		if time_segment == 0:
			t_ts = t_ts.filter(tournament__date_end__gte=models.SEASON_START)
		elif time_segment == 1:
			t_ts = t_ts.filter(tournament__date_end__gte=models.THREE_YEARS_BEFORE, tournament__date_end__lt=models.SEASON_START)
		else:
			t_ts = t_ts.filter(tournament__date_end__lt=models.THREE_YEARS_BEFORE)
		n_t_ts = t_ts.count()
		if debug:
			print(thousand, n_t_ts)

		if n_t_ts < 80000:
			for tournament_id, team_id in t_ts.values_list('tournament_id', 'team_id'):
				matrix[t_numbers_rev[tournament_id], team_numbers_rev[team_id]] = 1
		else:
			for t_thousand in range(t_max_thousand):
				t_id_from = t_thousand * 1000 + 1
				t_id_to = (t_thousand + 1) * 1000
				for tournament_id, team_id in t_ts.filter(tournament_id__gte=t_id_from, tournament_id__lte=t_id_to).values_list(
						'tournament_id', 'team_id'):
					matrix[t_numbers_rev[tournament_id], team_numbers_rev[team_id]] = 1

# Adds 1's for given time segment to the matrix returned by get_tt_player_lists
def update_tt_player_matrix(matrix, tt_numbers, tt_numbers_rev, player_numbers_rev, time_segment, debug=False):
	max_tt_thousand = (models.Team_on_tournament.objects.order_by('-pk').first().id - 1) // 1000 + 1
	max_player_thousand = (models.Player.objects.order_by('-pk').first().id - 1) // 1000 + 1
	for player_thousand in range(max_player_thousand):
		if debug:
			print(unicode(datetime.datetime.now()), 'Players thousand:', player_thousand)
		player_id_from = player_thousand * 1000 + 1
		player_id_to = (player_thousand + 1) * 1000

		p_ts = models.Player_on_tournament.objects.filter(
			team_on_tournament_id__in=set(tt_numbers), player_id__gte=player_id_from, player_id__lte=player_id_to)
		if time_segment == 0:
			p_ts = p_ts.filter(team_on_tournament__tournament__date_end__gte=models.SEASON_START)
		elif time_segment == 1:
			p_ts = p_ts.filter(team_on_tournament__tournament__date_end__gte=models.THREE_YEARS_BEFORE,
				team_on_tournament__tournament__date_end__lt=models.SEASON_START)
		else:
			p_ts = p_ts.filter(team_on_tournament__tournament__date_end__lt=models.THREE_YEARS_BEFORE)
		n_p_ts = p_ts.count()
		if debug:
			print(player_thousand, n_p_ts)

		if n_p_ts < 50000:
			for tt_id, player_id in p_ts.values_list('team_on_tournament_id', 'player_id'):
				matrix[tt_numbers_rev[tt_id], player_numbers_rev[player_id]] = 1
		else:
			for tt_thousand in range(max_tt_thousand):
				tt_id_from = tt_thousand * 1000 + 1
				tt_id_to = (tt_thousand + 1) * 1000
				for tt_id, player_id in p_ts.filter(team_on_tournament_id__gte=tt_id_from, team_on_tournament_id__lte=tt_id_to).values_list(
						'team_on_tournament_id', 'player_id'):
					matrix[tt_numbers_rev[tt_id], player_numbers_rev[player_id]] = 1

def get_index_value_list(matrix, row):
	row_coo = matrix[row, :].tocoo()
	return [(row_coo.col[i], row_coo.data[i]) for i in range(len(row_coo.data)) if row_coo.col[i] != row]

def fill_most_often_team_partners(only_last_part=False, debug=False):
	models.write_log("{} MOST OFTEN TEAM PARTNERS started".format(datetime.datetime.now()), debug=debug)
	if only_last_part:
		n_deleted = models.Team_most_often_partner.objects.filter(only_offline_games=False, time_segment=2).delete()
	else:
		n_deleted = models.Team_most_often_partner.objects.all().delete()
	if debug:
		print(datetime.datetime.now(), 'Deleted:', n_deleted)

	objs = []
	offline_values = [False] if only_last_part else (True, False)
	time_segments = [2] if only_last_part else range(3)
	for only_offline in offline_values:
		if debug:
			print(datetime.datetime.now(), "only_offline", only_offline)
		matrix, t_numbers, t_numbers_rev, team_numbers, team_numbers_rev = get_team_tournament_lists(only_offline)
		for time_segment in time_segments:
			if debug:
				print(datetime.datetime.now(), "time_segment", time_segment)
			update_t_t_matrix(matrix, t_numbers, t_numbers_rev, team_numbers_rev, 3 if only_last_part else time_segment)
			games = sp.csr_matrix(matrix, dtype=np.uint16)
			if debug:
				print(datetime.datetime.now(), "Games matrix:", games.shape)
			counts = games.T.dot(games)
			if debug:
				print(datetime.datetime.now(), "Counts csr_matrix:", counts.shape)
			# counts_array = counts.toarray()
			# print(datetime.datetime.now(), "Counts array:", counts_array.shape)

			for team1_number in range(len(team_numbers)): # [team_numbers_rev[56459]]:
				index_value_list = get_index_value_list(counts, team1_number)
				for team2_number, n_games in sorted(index_value_list, key=lambda x:-x[1])[:10]:
					objs.append(models.Team_most_often_partner(
							team_id=team_numbers[team1_number],
							team2_id=team_numbers[team2_number],
							time_segment=time_segment,
							only_offline_games=only_offline,
							n_games=n_games,
						))
				if len(objs) >= 10000:
					if debug:
						print(datetime.datetime.now(), f'Team {team_numbers[team1_number]}. Saving {len(objs)} records to DB...')
					models.Team_most_often_partner.objects.bulk_create(objs)
					del objs[:]
					if debug:
						print(datetime.datetime.now(), 'Saved.')

	models.Team_most_often_partner.objects.bulk_create(objs)
	set_param_value('team_partners_filled', datetime.date.today())
	models.write_log("{} MOST OFTEN TEAM PARTNERS finished".format(datetime.datetime.now()), debug=debug)
	if debug:
		print(datetime.datetime.now(), 'Done')

def print_now(s):
	print(datetime.datetime.now(), s)

def fill_player_partners(only_last_part=False, debug=False):
	models.write_log("{} PLAYER PARTNERS started".format(datetime.datetime.now()), debug=debug)
	if only_last_part:
		n_deleted = models.Player_partner.objects.filter(only_offline_games=False, time_segment=2).delete()
	else:
		n_deleted = models.Player_partner.objects.all().delete()
	if debug:
		print_now('Deleted: {}'.format(n_deleted))

	objs = []
	offline_values = [False] if only_last_part else (True, False)
	time_segments = [2] if only_last_part else range(3)
	for only_offline in offline_values:
		if debug:
			print_now('only_offline: {}'.format(only_offline))
		# matrix, t_numbers, t_numbers_rev, team_numbers, team_numbers_rev = get_tt_player_lists(only_offline)
		matrix, tt_numbers, tt_numbers_rev, player_numbers, player_numbers_rev = get_tt_player_lists(only_offline)
		for time_segment in time_segments:
			if debug:
				print_now('time_segment: {}'.format(time_segment))
			update_tt_player_matrix(matrix, tt_numbers, tt_numbers_rev, player_numbers_rev, 3 if only_last_part else time_segment, debug=debug)
			games = sp.csr_matrix(matrix, dtype=np.uint16)
			if debug:
				print_now('Games matrix: {}'.format(games.shape))
			counts = games.T.dot(games)
			if debug:
				print_now('Counts csr_matrix: {}'.format(counts.shape))

			for player1_number in range(len(player_numbers)): # [player_numbers_rev[56459]]: # range(n_teams):
				index_value_list = get_index_value_list(counts, player1_number)
				for player2_number, n_games in index_value_list: # sorted(index_value_list, key=lambda x:-x[1])[:10]:
					if n_games > 0:
						objs.append(models.Player_partner(
								player_id=player_numbers[player1_number],
								player2_id=player_numbers[player2_number],
								time_segment=time_segment,
								only_offline_games=only_offline,
								n_games=n_games,
							))
				if len(objs) >= 10000:
					print_now('Player id: {}. Adding objects: {}'.format(player1_number, len(objs)))
					models.Player_partner.objects.bulk_create(objs)
					print_now('Added!')
					del objs[:]

	models.Player_partner.objects.bulk_create(objs)
	set_param_value('player_partners_filled', datetime.date.today())
	models.write_log("{} PLAYER PARTNERS finished".format(datetime.datetime.now()), debug=debug)
	if debug:
		print(datetime.datetime.now(), 'Done')

def fill_players_in_teams(debug=False):
	models.write_log("{} PLAYERS IN TEAMS started".format(datetime.datetime.now()), debug=debug)
	team_ids = set(models.Team.objects.exclude(name='').values_list('pk', flat=True))

	player_teams = Counter()
	prev_first_date = datetime.date.today() + datetime.timedelta(days=365)
	for time_segment, segment_name, first_date in models.TIME_SEGMENTS:
		n_deleted, _ = models.Player_in_team.objects.filter(time_segment=time_segment).delete()
		if debug:
			print("Time segment:", segment_name, '. Deleted from Player_in_team:', n_deleted)

		p_ts = models.Player_on_tournament.objects.filter(team_on_tournament__tournament__is_in_rating=True,
			team_on_tournament__tournament__date_end__gte=first_date, team_on_tournament__tournament__date_end__lt=prev_first_date)
		print("Size of p_ts:", p_ts.count())
		max_thousand = (models.Tournament.objects.order_by('-pk').first().id - 1) // 1000 + 1
		for thousand in range(max_thousand):
			print('Tournament thousand:', thousand)
			id_from = thousand * 1000 + 1
			id_to = (thousand + 1) * 1000
			for player_id, team_id in p_ts.filter(team_on_tournament__tournament_id__gte=id_from,
					team_on_tournament__tournament_id__lte=id_to).values_list('player_id', 'team_on_tournament__team_id'):
				if team_id in team_ids:
					player_teams[(player_id, team_id)] += 1
				else:
					print('Unknown team:', team_id)
		
		record = player_teams.most_common(1)[0]
		print("Size:", len(player_teams), "Most number of games (player, team, games):", record[0][0], record[0][1], record[1])
		items = list(player_teams.items())
		n_parts = (len(items) - 1) // 10000 + 1
		for i in range(n_parts):
			objs = [models.Player_in_team(player_id=p_t[0], team_id=p_t[1], time_segment=time_segment, n_games=n_games)
						for p_t, n_games in items[i * 10000:(i+1) * 10000]]
			models.Player_in_team.objects.bulk_create(objs)
		print('Created!')
		prev_first_date = first_date
	set_param_value('players_in_teams', datetime.date.today())
	models.write_log("{} PLAYERS IN TEAMS finished".format(datetime.datetime.now()), debug=debug)

def get_finished_tournament_conditions():
	return {
			'tournament__is_in_rating': True,
			'tournament__n_teams_with_results__gt': 0,
			'tournament__t_type__in': models.IN_RATING_TYPES,
			}

def get_finished_rating_t_t_with_results():
	return models.Team_on_tournament.objects.filter(**get_finished_tournament_conditions()).select_related('team', 'tournament')

def find_all_totals(debug=False):
	res = get_finished_rating_t_t_with_results().filter(
		n_questions_taken=F('tournament__questions_total')-F('tournament__questions_dropped')).order_by('tournament__date_end')
	if debug:
		for t_t in res:
			print(t_t.tournament.id, t_t.tournament.name, t_t.team.id, t_t.team.name)
	return ((t_t, t_t.get_players_list_asc()) for t_t in res)

def find_t_t_that_saved_question(tournament, question_number):
	block_number, number_in_block = divmod(question_number - 1, models.N_QUESTIONS_IN_BLOCK)
	for block in models.Questions_taken.objects.filter(team_on_tournament__tournament=tournament, block_number=block_number):
		bit = (block.bits >> (block.n_questions - number_in_block - 1)) & 1
		if bit:
			return block.team_on_tournament
	return None

def find_saved_by_one_team(today, last_date_end):
	res = []
	for question in models.Question_data.objects.filter(n_teams_taken=1, tournament__date_end__lte=last_date_end,
			**get_finished_tournament_conditions()).order_by('-tournament__n_teams_with_results')[:4]:
		t_t = find_t_t_that_saved_question(question.tournament, question.number)
		question_text = question.tournament.question_text_set.filter(number=question.number).first()
		res.append((t_t, t_t.get_players_list_asc(), question.number, question_text))
	return res

def find_coffins(today, last_date_end):
	res = []
	# BAD_TOURNAMENT_IDS = [1754, 1853, 1854, 1931, 2166, 2578, ]
	BAD_TOURNAMENT_IDS = [1854, 1931, 2166, 2578, ]
	for question in models.Question_data.objects.filter(n_teams_taken=0, is_dropped=False, tournament__date_end__lte=last_date_end,
			**get_finished_tournament_conditions()).exclude(tournament_id__in=BAD_TOURNAMENT_IDS).order_by(
			'-tournament__n_teams_with_results')[:5]:
		question_text = question.tournament.question_text_set.filter(number=question.number).first()
		res.append((question, question_text))
	return res

def get_questions_played_by_team(today, first_date_end, last_date_end):
	# games_played = Counter()
	questions_played = Counter()
	questions_not_dropped = Counter()
	questions_taken = Counter()
	for team_id, n_questions_total, n_questions_dropped, n_questions_taken in models.Team_on_tournament.objects.filter(is_included_in_rating=True,
			tournament__date_end__gte=first_date_end, tournament__date_end__lte=last_date_end, **get_finished_tournament_conditions()).values_list(
			'team_id', 'tournament__questions_total', 'tournament__questions_dropped', 'n_questions_taken'):
		# games_played[team_id] += 1
		questions_played[team_id] += n_questions_total
		questions_not_dropped[team_id] += n_questions_total - n_questions_dropped
		questions_taken[team_id] += n_questions_taken

	all_rating_t_ts = models.Team_on_tournament.objects.filter(is_included_in_rating=True, tournament__date_end__gte=first_date_end,
		tournament__date_end__lte=last_date_end, tournament__is_in_rating=True,
		tournament__t_type__in=models.IN_RATING_TYPES).values_list('team_id', flat=True)
	games_played = Counter(all_rating_t_ts)
	return games_played, questions_played, questions_not_dropped, questions_taken

# How many good tournaments per year does MGTU have? A problem with tournaments that end before 10.09.2006 was found
def check_n_good_tournaments():
	team = models.Team.objects.get(pk=673)
	for year in range(1995,2019):
		t_ts = team.team_on_tournament_set.filter(is_included_in_rating=True, tournament__date_end__gte=datetime.date(year, 9, 1),
			tournament__date_end__lte=datetime.date(year + 1, 8, 31), tournament__is_in_rating=True)
		print(year, t_ts.count())
		if t_ts.count():
			c = Counter(list(t_ts.values_list('tournament__t_type', flat=True)))
			for key, val in c.most_common():
				print(key, ': ', val)


N_TEAM_LINES = 10
def get_team_ids_with_best_percent(questions_not_dropped, questions_taken, min_games=0):
	percents = []
	for team_id, n_questions_not_dropped in questions_not_dropped.items():
		if n_questions_not_dropped >= min_games:
			percents.append(
				(team_id, questions_taken[team_id], n_questions_not_dropped, (100 * questions_taken[team_id]) / n_questions_not_dropped))
	return sorted(percents, key=lambda x: -x[3])[:N_TEAM_LINES]

def synch_request_id2venue(synch_request_id):
	return models.Team_on_tournament.objects.filter(synch_request_id=synch_request_id).first().venue

def get_largest_venues():
	venues = Counter()
	for t_t in models.Team_on_tournament.objects.filter(tournament__t_type__in=(3, 6)).exclude(venue=None).exclude(
			tournament__id__in=(2716, 4663, )).values(
			'tournament_id', 'synch_request_id'):
		venues[(t_t['tournament_id'], t_t['synch_request_id'])] += 1
	return [(models.Tournament.objects.get(pk=k[0]), synch_request_id2venue(k[1]), v) for k, v in venues.most_common(N_TEAM_LINES)]

def generate_stat_misc():
	context = {}
	today = datetime.date.today()
	last_date_end = today - datetime.timedelta(days=30)
	context['today'] = models.date2str(today)
	context['last_date_end'] = models.date2str(last_date_end)
	context['totals'] = find_all_totals()
	context['saved_by_one_team'] = find_saved_by_one_team(today, last_date_end)
	context['coffins'] = find_coffins(today, last_date_end)

	context['teams_records_data'] = []
	for time_segment, segment_name, first_date in models.TIME_SEGMENTS:
		games_played, questions_played, questions_not_dropped, questions_taken = get_questions_played_by_team(today, first_date, last_date_end)
		data = {}
		data['segment_name'] = segment_name
		most_games = [(models.Team.objects.get(pk=team_id), n_games)
			for team_id, n_games in games_played.most_common(N_TEAM_LINES)]
		most_questions_played = [(models.Team.objects.get(pk=team_id), n_questions)
			for team_id, n_questions in questions_played.most_common(N_TEAM_LINES)]
		most_questions_taken = [(models.Team.objects.get(pk=team_id), n_questions)
			for team_id, n_questions in questions_taken.most_common(N_TEAM_LINES)]
		most_questions_percent = [(models.Team.objects.get(pk=team_id), n_questions_taken, n_questions_not_dropped, percent)
			for team_id, n_questions_taken, n_questions_not_dropped, percent
			in get_team_ids_with_best_percent(questions_not_dropped, questions_taken)]
		data['min_games'] = (time_segment + 1) * 100
		most_questions_percent_2 = [(models.Team.objects.get(pk=team_id), n_questions_taken, n_questions_not_dropped, percent)
			for team_id, n_questions_taken, n_questions_not_dropped, percent
			in get_team_ids_with_best_percent(questions_not_dropped, questions_taken, data['min_games'])]

		data['lines'] = []
		for i in range(N_TEAM_LINES):
			data['lines'].append({
					'most_games': most_games[i],
					'most_questions_played': most_questions_played[i],
					'most_questions_taken': most_questions_taken[i],
					'most_questions_percent': most_questions_percent[i],
					'most_questions_percent_2': most_questions_percent_2[i],
				})
		context['teams_records_data'].append(data)

	context['largest_offline_tournaments'] = models.Tournament.objects.filter(
		t_type__in=models.IN_RATING_OFFLINE_TYPES).exclude(questions_total__gte=150).exclude(pk__in=(498, 3000, 3488)).order_by(
		'-n_teams_participated')[:N_TEAM_LINES]
	context['largest_synchrons'] = models.Tournament.objects.filter(
		t_type__in=models.IN_RATING_SYNCHRON_TYPES).exclude(questions_total__gte=150).exclude(pk__in=(219, 314, 483, 617, 1535)).order_by(
		'-n_teams_participated')[:N_TEAM_LINES]
	context['largest_venues'] = get_largest_venues()

	with io.open(os.path.join(settings.BASE_DIR, 'comparing/templates/generated/stat_misc.html'), 'w', encoding="utf8") as output_file:
		output_file.write(render_to_string('generators/stat_misc.html', context))

def fill_ratings_for_mansur():
	with io.open(models.BASE_DIR + "data/2019-08-29-one-team-per-place.csv", "r") as f_in:
		with io.open(models.BASE_DIR + "data/2019-08-29-for-mansur.csv", "w") as f_out:
			line_number = 0
			for line in f_in:
				tournament_id, a, b, team_id = line.strip('\n').split(';')
				t_t = models.Team_on_tournament.objects.filter(team_id=team_id, tournament_id=tournament_id).first()
				if t_t:
					f_out.write(';'.join([unicode(x) for x in (tournament_id, a, b, team_id, t_t.diff_bonus)]) + '\n')
				else:
					print('Team_id {}, tournament_id {} do not exist!'.format(team_id, tournament_id))
				# line_number += 1
				# if line_number == 10:
				# 	break
	print('Done!')

def get_tournament_matrix(tournament):
	t_ts = tournament.team_on_tournament_set
	team_ids = sorted(t_ts.values_list('team_id', flat=True))
	n_teams = len(team_ids)
	team_ids_rev = {team_ids[i]: i for i in range(n_teams)}

	n_questions = tournament.questions_total
	matrix = np.zeros((n_teams + 1, n_questions), dtype=int)
	for questions_taken in models.Questions_taken.objects.filter(team_on_tournament__tournament=tournament).select_related('team_on_tournament'):
		team_id = questions_taken.team_on_tournament.team_id
		if team_id not in team_ids_rev:
			print('Tournament_id {}: team_id {} is found in Questions_taken but not in team_on_tournament_set. Skipping'.format(
				tournament.id, team_id))
			continue
		for question_number_in_block, res in enumerate(questions_taken.asArray()):
			if res:
				question_number = models.N_QUESTIONS_IN_BLOCK * questions_taken.block_number + question_number_in_block
				matrix[team_ids_rev[team_id], question_number] = 1
				matrix[n_teams, question_number] += 1
	return team_ids, team_ids_rev, matrix

def get_corrs(tournament):
	team_ids, team_ids_rev, matrix = get_tournament_matrix(tournament)
	n_teams = len(team_ids)
	n_questions = tournament.questions_total

	data = []
	for t_t in tournament.team_on_tournament_set.select_related('team').order_by('team_id'):
		team = t_t.team
		team_number = team_ids_rev[team.id]
		corr = np.corrcoef(matrix[team_number, :], matrix[n_teams, :])[0,1]
		if not np.isnan(corr):
			data.append((team, t_t, corr))
	return data

def find_strange_corrs():
	tournaments = models.Tournament.objects.filter(is_in_rating=True, date_end__year=2019, questions_total__gt=0).order_by('pk')
	print('Tournaments:', tournaments.count())
	for tournament in tournaments:
		for team, t_t, corr in get_corrs(tournament):
			if (corr < .15) and ((2 * t_t.n_questions_taken) >= tournament.questions_total) and (t_t.diff_bonus > 200):
				print(tournament.id, tournament.name, team.id, team.name, tournament.questions_total, t_t.n_questions_taken, t_t.diff_bonus, corr)
	print('Done!')

def all_team_pairs_for_andy(year):
	with io.open('20200211_team_pairs_{}_small.csv'.format(year), 'w', encoding='utf8') as f:
		f.write(u'ID турнира;Дата конца;Команда 1: ID;Преемственный состав?;RG;Взято;Команда 2: ID;Преемственный состав?;RG;Взято\n')
		n_tournaments = 0
		n_lines = 0
		for tournament in models.Tournament.objects.filter(is_in_rating=True, date_end__year=year, questions_total__gt=0).exclude(
				pk=6123).order_by('date_end', 'pk'):
			t_ts = list(tournament.team_on_tournament_set.select_related('team').order_by('pk'))
			for j in range(len(t_ts)):
				team2 = t_ts[j].team
				for i in range(j):
					team1 = t_ts[i].team
					data = [tournament.id,
							# tournament.name,
							# tournament.date_start,
							tournament.date_end,
							# tournament.t_type,
							team1.id,
							# team1.name,
							int(t_ts[i].is_included_in_rating),
							t_ts[i].tech_rating_rg,
							t_ts[i].n_questions_taken,
							team2.id,
							# team2.name,
							int(t_ts[j].is_included_in_rating),
							t_ts[j].tech_rating_rg,
							t_ts[j].n_questions_taken,
						]
					f.write(u';'.join(unicode(x) for x in data) + '\n')
			n_tournaments += 1
			n_lines += (len(t_ts) * (len(t_ts) - 1)) // 2
			if (n_tournaments % 100) == 0:
				print(n_tournaments, tournament.id)
	print('Done! {} {}'.format(n_tournaments, n_lines))

def get_tournaments_with_rg_none(year):
	tournament_ids = set(models.Team_on_tournament.objects.filter(tournament__is_in_rating=True, tournament__date_end__year=year,
		tech_rating_rg=None).values_list('tournament_id', 'team_id', 'team__name'))
	print(sorted(tournament_ids))

def tts_per_year():
	for year in range(2010, 2021):
		tournaments = set(models.Tournament.objects.exclude(name__istartswith='Онлайн:').filter(date_start__year=year).values_list('pk', flat=True))
		tts = models.Team_on_tournament.objects.filter(tournament_id__in=tournaments)
		print(year, len(tournaments), tts.count())

def fill_team_close_to_mine(debug=0):
	models.write_log(f'{datetime.datetime.now()} TEAM CLOSE TO MINE started', debug=debug)
	models.Team_close_to_mine.objects.all().delete()
	year_ago = datetime.date.today() - datetime.timedelta(days=366)
	last_year_results = models.Team_on_tournament.objects.filter(tournament__date_end__gte=year_ago, tournament__is_in_rating=True).exclude(n_questions_taken=None)
	tournaments_by_team = defaultdict(list) # For each team, stores the list or tournament IDs played, and score
	teams_by_tournament = defaultdict(list) # For each tournament, stores the list or team IDs that played it, and score
	for team_id, tournament_id, date_end, n_questions_taken in last_year_results.order_by('-tournament__date_end').values_list(
			'team_id', 'tournament_id', 'tournament__date_end', 'n_questions_taken'):
		tournaments_by_team[team_id].append((tournament_id, n_questions_taken))
		teams_by_tournament[tournament_id].append((team_id, n_questions_taken))
	teams_in_scope = set(team_id for team_id, tournaments in tournaments_by_team.items() if (len(tournaments) >= models.MIN_GAMES_IN_YEAR_TO_CHECK))
	if debug:
		print(f'Teams with at least {models.MIN_GAMES_IN_YEAR_TO_CHECK} rating games last year:', len(teams_in_scope))
	
	models.Team.objects.all().update(n_games_last_year=None, n_teams_played_many_games_with_me=None)

	objs = []
	for team_id in teams_in_scope:
		diffs_by_team2 = defaultdict(list) # For each other team, we collect all differences of scores from tournaments where team_id played.
		for tournament_id, team_score in tournaments_by_team[team_id]:
			for team2_id, team2_score in teams_by_tournament[tournament_id]:
				if team2_id != team_id:
					diffs_by_team2[team2_id].append(team_score - team2_score)

		# For each other team, we collect expectation and variance over last MAX_TOGETHER_GAMES_TO_CHECK, along with
		# total number of games this team and team_id played last year.
		exp_var_ngames = []
		for team2_id, diffs in diffs_by_team2.items():
			if len(diffs) < models.MIN_GAMES_IN_YEAR_TO_CHECK:
				continue
			diffs_in_scope = diffs[:models.MAX_TOGETHER_GAMES_TO_CHECK]
			my_mean = np.mean(diffs_in_scope)
			exp_var_ngames.append((abs(my_mean), np.var(diffs_in_scope), len(diffs), my_mean, team2_id))

		if not exp_var_ngames:
			continue
		models.Team.objects.filter(pk=team_id).update(n_games_last_year=len(tournaments_by_team[team_id]), n_teams_played_many_games_with_me=len(exp_var_ngames))
		for abs_mean, my_var, n_common_games, my_mean, team2_id in sorted(exp_var_ngames)[:models.CLOSEST_TEAMS_TO_STORE]:
			objs.append(models.Team_close_to_mine(
				team_id=team_id,
				team2_id=team2_id,
				games_both_played=n_common_games,
				difference=my_mean,
				abs_difference=abs_mean,
				deviation=my_var,
			))
		if len(objs) >= 10000:
			if debug:
				print(datetime.datetime.now(), f'Team {team_id}. Saving {len(objs)} records to DB...')
			models.Team_close_to_mine.objects.bulk_create(objs)
			del objs[:]
			if debug:
				print(datetime.datetime.now(), 'Saved.')
	models.Team_close_to_mine.objects.bulk_create(objs)
	set_param_value('team_close_to_mine', datetime.date.today())
	models.write_log(f'{datetime.datetime.now()} TEAM CLOSE TO MINE finished', debug=debug)
