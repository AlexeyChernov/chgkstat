from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse
from django.http import HttpResponse

import json

from comparing import models

MAX_ENTRIES = 20
MIN_QUERY_LENGTH = 3

def tournament_list(request):
	results = []
	query = request.GET.get('query', '')
	if len(query) >= MIN_QUERY_LENGTH:
		tournament_id = models.int_safe(query)
		if tournament_id > 0:
			tournament = models.Tournament.objects.filter(pk=tournament_id).first()
			if tournament:
				results.append(tournament.get_json_for_select())
		for tournament in models.Tournament.objects.filter(name__icontains=query).order_by('-date_start')[:MAX_ENTRIES]:
			results.append(tournament.get_json_for_select())
	return HttpResponse(json.dumps(results), content_type='application/json')

def team_list(request):
	results = []
	query = request.GET.get('query', '')
	if len(query) >= MIN_QUERY_LENGTH:
		team_id = models.int_safe(query)
		if team_id > 0:
			team = models.Team.objects.filter(pk=team_id).first()
			if team:
				results.append(team.get_json_for_select())
		for team in models.Team.objects.filter(name=query).select_related('city').order_by('-n_games_last_year', 'name')[:MAX_ENTRIES]:
			results.append(team.get_json_for_select())
		for team in models.Team.objects.filter(name__icontains=query).exclude(name=query).select_related('city').order_by('-n_games_last_year', 'name')[:MAX_ENTRIES]:
			results.append(team.get_json_for_select())
	return HttpResponse(json.dumps(results), content_type='application/json')
