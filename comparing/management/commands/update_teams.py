from django.core.management.base import BaseCommand
import datetime

from comparing import rating_api, views_stat

class Command(BaseCommand):
	help = 'Updates Team table'

	def add_arguments(self, parser):
		parser.add_argument('--teams_page_from', type=int, help='From what page to start updating teams', default=1)
		parser.add_argument('--players_page_from', type=int, help='From what page to start updating players', default=1)
		parser.add_argument('--debug', type=int, default=0)
		parser.add_argument('--recent_tournaments_from', type=int, default=0)
		parser.add_argument('--max_to_update', type=int, default=0)
		parser.add_argument('--most_often_team_partners_only_last_part', action='store_true')

	def handle(self, *args, **options):
		rating_api.update_countries(debug=options['debug'])
		rating_api.update_regions(debug=options['debug'])
		rating_api.update_cities(debug=options['debug'])
		rating_api.update_venues(debug=options['debug'])
		rating_api.update_teams(debug=options['debug'], page_from=options['teams_page_from'])
		rating_api.update_tournaments(debug=options['debug'])
		rating_api.update_players(page_from=options['players_page_from'], debug=options['debug'])
		rating_api.update_tournaments_completely(debug=options['debug'])
		rating_api.update_teams_rating(debug=options['debug'])
		# rating_api.update_tournaments_completely(
		# 	id_from=options['recent_tournaments_from'],
		# 	date_end_from=datetime.date(1900,1,1),
		# 	max_to_update=options['max_to_update'],
		# 	debug=options['debug'],
		# )

		if datetime.date.today().weekday() == 4:
		# if 1:
			views_stat.fill_players_in_teams(debug=options['debug'])
			views_stat.fill_most_often_team_partners(debug=options['debug'])
			views_stat.fill_team_close_to_mine(debug=options['debug'])
			# views_stat.fill_most_often_team_partners(only_last_part=options['most_often_team_partners_only_last_part'], debug=options['debug'])
			# views_stat.fill_team_ratings()
