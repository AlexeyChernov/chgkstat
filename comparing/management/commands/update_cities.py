from django.core.management.base import BaseCommand

from comparing import models
from comparing.views_stat import update_cities

class Command(BaseCommand):
	help = 'Updates Team table'

	def handle(self, *args, **options):
		update_cities(debug=True)
