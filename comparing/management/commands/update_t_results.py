from django.core.management.base import BaseCommand

import datetime

from comparing import models, rating_api

class Command(BaseCommand):
	help = 'Updates Tournament results'

	def add_arguments(self, parser):
		parser.add_argument('tournament_id')
		parser.add_argument('--debug', type=int, default=0)

	def handle(self, *args, **options):
		all_teams_in_base = {team.id: team for team in models.Team.objects.all()}
		rating_api.update_tournament_and_results(models.Tournament.objects.get(pk=options['tournament_id']), debug=options['debug'], all_teams=all_teams_in_base)
