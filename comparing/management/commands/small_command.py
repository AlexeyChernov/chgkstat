from django.core.management.base import BaseCommand

from comparing import models, views, views_stat, rating_api

class Command(BaseCommand):
	help = 'Updates Team table'

	def handle(self, *args, **options):
		# views_stat.fill_team_close_to_mine(debug=1)
		# rating_api.update_teams_rating(debug=1)
		# views._clear_cache()
		# rating_api.update_tournaments_completely(debug=1)
		rating_api.update_tournament_and_results(models.Tournament.objects.get(pk=9193), debug=2)