from django.template.loader import render_to_string
from django.db.models import Max, Sum, F, Q
from django.shortcuts import render
from django.conf import settings

import datetime
import time
from decimal import Decimal
from typing import Dict, Optional

from comparing import models, comparing_util

def update_teams(debug=False, page_from=1):
	models.write_log("{} UPDATE TEAMS started".format(datetime.datetime.now()), debug=debug)
	all_teams_in_base = {team.id: team for team in models.Team.objects.all()}
	# all_teams_in_base = set(models.Team.objects.all().values_list('id'))
	n_created = 0
	n_updated = 0
	n_deleted = 0
	n_pages_broken = 0
	page = 0
	while n_pages_broken < 5:
		page += 1
		url = models.Team.get_list_json_url(page)
		try:
			page_json = comparing_util.url2json(url)
		except:
			models.write_log(f'Teams page broken: {url}', debug=debug)
			n_pages_broken += 1
			continue
		if len(page_json) == 0:
			break
		for team_json in page_json:
			small_json = {
				'id': team_json['id'],
				'city_id': team_json['town']['id'],
				'name': team_json['name'],
			}
			team_id = team_json['id']
			if team_id in all_teams_in_base:
				team = all_teams_in_base.pop(team_id)
				if team.update_if_needed(small_json, comment='Обновление всех команд'):
					n_updated += 1
			else:
				models.Team.create_new(small_json, comment='Обнаружили новую команду при обновлении')
				n_created += 1
		# print('Page', page, 'is processed')
	if page_from == 1 and n_pages_broken == 0:
		for team_id in all_teams_in_base:
			team = models.Team.objects.get(pk=team_id)
			if team.update_if_needed({'name': '', 'city_name': ''}, comment='Не встретили команду при обновлении'):
				n_deleted += 1
	if n_created + n_updated + n_deleted:
		models.fill_team_geos()
	models.write_log("{} UPDATE TEAMS finished. Broken pages: {}. New teams: {}, updated teams: {}, deleted teams: {}".format(
		datetime.datetime.now(), n_pages_broken, n_created, n_updated, n_deleted), debug=debug)
	models.set_stat_value(name='n_teams', value=models.Team.objects.exclude(name='').count())

def update_tournaments(debug=False):
	models.write_log("{} UPDATE TOURNAMENTS started".format(datetime.datetime.now()), debug=debug)
	all_tournaments_in_base = {tournament.id: tournament for tournament in models.Tournament.objects.all()}
	tournaments_processed = set()
	n_created = 0
	n_not_created = 0
	n_updated = 0
	n_deleted = 0
	n_tournaments_broken = 0
	today = datetime.date.today() + datetime.timedelta(days=2)
	page = 0
	while n_tournaments_broken <= 5:
		page += 1
		page_json = comparing_util.url2json(models.Tournament.get_list_json_url(page))
		if len(page_json) == 0:
			break
		for tournament_json in page_json:
			try:
				if datetime.datetime.fromisoformat(tournament_json['dateStart']).date() > today:
					continue
				if tournament_json['id'] in tournaments_processed:
					models.write_log(f'We found tournament {j["id"]} on page {page} but we already processed it! Skipping', debug=debug)
					continue
				tournaments_processed.add(tournament_json['id'])

				processed_json = models.Tournament.get_dict_from_json(tournament_json)
				if tournament_json['id'] in all_tournaments_in_base:
					tournament = all_tournaments_in_base.pop(tournament_json['id'])
					if tournament.update_if_needed(processed_json, comment='Обновление всех турниров'):
						n_updated += 1
				else:
					if debug and (tournament_json['id'] == 8692):
						print(f"{tournament_json['id']} is being created!")
					tournament = models.Tournament.create_new(processed_json, debug=debug, comment='Обнаружили новый турнир при обновлении')
					if tournament:
						n_created += 1
					else:
						n_not_created += 1
			except Exception as e:
				models.write_log(f"Could not load tournament {tournament_json['id']}: {e}", debug=debug)
				n_tournaments_broken += 1
	if n_tournaments_broken == 0:
		for tournament_id in all_tournaments_in_base:
			tournament = models.Tournament.objects.get(pk=tournament_id)
			if tournament.update_if_needed({'name': '', 'type_name': '', 'date_start': None, 'date_end': None,
					'is_in_rating': False}, comment='Не встретили турнир при обновлении'):
				n_deleted += 1
	models.write_log("{} UPDATE TOURNAMENTS finished. Broken tournaments: {}. New: {}, not created: {}, updated: {}, deleted: {}".format(
		datetime.datetime.now(), n_tournaments_broken, n_created, n_not_created, n_updated, n_deleted), debug=debug)
	models.set_stat_value(name='n_tournaments', value=models.Tournament.objects.exclude(name='').count())

def update_simple_objs(cls, plural, load_cities=False, debug=False):
	models.write_log('{} UPDATE {} started'.format(datetime.datetime.now(), plural.upper()), debug=debug)
	cls_name = cls.__name__
	n_created = 0
	n_updated = 0
	n_deleted = 0
	obj_ids = set()
	if load_cities:
		existing_cities = {x.id: x for x in models.City.objects.all()}

	url = cls.get_list_json_url()
	try:
		page_json = comparing_util.url2json(url)
	except:
		models.write_log('{} page broken: {}'.format(plural, url), debug=debug)
		return

	for obj_json in page_json:
		obj_id = obj_json['id']
		obj_ids.add(obj_id)

		obj_dict = {'name': obj_json['name']}
		if load_cities:
			obj_dict['city'] = None
			if 'town' in obj_json:
				# We fill the city only if it already exists in our DB (i.e., in existing_cities)
				obj_dict['city'] = existing_cities.get(obj_json['town'].get('id'))

		obj, created = cls.objects.get_or_create(pk=obj_id, defaults=obj_dict)
		if created:
			models.write_log('New {} is created: {} (id {})'.format(cls_name, obj.name, obj.id), debug=debug)
			n_created += 1
		else:
			if obj.name != obj_dict['name']:
				models.write_log('{} name is changed! Id: {}, old: {}, new: {}'.format(cls_name, obj.id, obj.name, obj_json['name']), debug=debug)
			if models.update_obj_if_needed(obj, obj_dict):
				n_updated += 1

	empty_dict = {'name': ''}
	if load_cities:
		empty_dict['city'] = None
	if obj_ids: # We delete objects only if some objects were found now
		for obj in cls.objects.exclude(pk__in=obj_ids).exclude(name='').order_by('pk'):
			models.write_log('{} is deleted: {} (id {})'.format(cls_name, obj.name, obj.id), debug=debug)
			models.update_obj_if_needed(obj, empty_dict, comment='Не встретили при обновлении')
			n_deleted += 1

	models.write_log('{} UPDATE {} finished. Created: {}, updated: {}, deleted: {}'.format(
		datetime.datetime.now(), plural.upper(), n_created, n_updated, n_deleted), debug=debug)
	models.set_stat_value(name='n_{}'.format(plural), value=cls.objects.count())

def update_countries(debug=False):
	update_simple_objs(models.Country, 'countries', debug=debug)

def update_venues(debug=False):
	update_simple_objs(models.Venue, 'venues', load_cities=True, debug=debug)

def update_regions(debug=False):
	models.write_log("{} UPDATE REGIONS started".format(datetime.datetime.now()), debug=debug)
	n_created = 0
	n_updated = 0
	n_deleted = 0
	regions_ids = set()
	url = models.Region.get_list_json_url()
	try:
		page_json = comparing_util.url2json(url)
	except:
		models.write_log("Regions page broken: {}".format(url), debug=debug)
		return

	for region_json in page_json:
		region_id = region_json['id']
		regions_ids.add(region_id)
		country_json = region_json.get('country')
		country_id = country_json['id'] if country_json else None
		region, created = models.Region.objects.get_or_create(pk=region_id,
			defaults={'name': region_json['name'], 'country_id': country_id})
		if created:
			models.write_log('New region is created: {} (id {}, country_id {})'.format(
				region.name, region.id, country_id), debug=debug)
			n_created += 1
		else:
			needs_update = False
			if region.name != region_json['name']:
				models.write_log('Region name is changed! Id: {}, old: {}, new: {}'.format(region.id, region.name, region_json['name']),
					debug=debug)
				needs_update = True
			if region.country_id != country_id:
				models.write_log('Region country is changed! Id: {}, old: {}, new: {}'.format(region.id, region.country_id, country_id),
					debug=debug)
				needs_update = True
			if needs_update:
				models.update_obj_if_needed(region, {'name': region_json['name'], 'country_id': country_id})
				n_updated += 1

	if regions_ids: # We delete regions only if some regions were found now
		for region in models.Region.objects.exclude(pk__in=regions_ids).exclude(name='').order_by('pk'):
			print('Region is deleted: {} (id {})'.format(region.name, region.id))
			models.update_obj_if_needed(region, {'name': '', 'country_id': None}, comment='Не встретили при обновлении')
			n_deleted += 1

	models.write_log('{} UPDATE REGIONS finished. Created: {}, updated: {}, deleted: {}'.format(
		datetime.datetime.now(), n_created, n_updated, n_deleted), debug=debug)
	models.set_stat_value(name='n_regions', value=models.Region.objects.count())

def update_cities(debug=False):
	models.write_log("{} UPDATE CITIES started".format(datetime.datetime.now()), debug=debug)
	n_created = 0
	n_updated = 0
	n_deleted = 0
	cities_ids = set()
	url = models.City.get_list_json_url()
	try:
		page_json = comparing_util.url2json(url)
	except:
		models.write_log("Cities page broken: {}".format(url), debug=debug)
		return

	for city_json in page_json:
		city_id = city_json['id']
		cities_ids.add(city_id)
		region_json = city_json.get('region')
		region_id = region_json['id'] if region_json else None
		country_json = city_json.get('country')
		country_id = country_json['id'] if country_json else None
		city, created = models.City.objects.get_or_create(pk=city_id,
			defaults={'name': city_json['name'], 'region_id': region_id, 'country_id': country_id})
		if created:
			models.write_log('New city is created: {} (id {}, region_id {}, country_id {})'.format(
				city.name, city.id, region_id, country_id), debug=debug)
			n_created += 1
		else:
			needs_update = False
			if city.name != city_json['name']:
				models.write_log('City name is changed! Id: {}, old: {}, new: {}'.format(city.id, city.name, city_json['name']), debug=debug)
				needs_update = True
			if city.region_id != region_id:
				models.write_log('City region is changed! Id: {}, old: {}, new: {}'.format(city.id, city.region_id, region_id), debug=debug)
				needs_update = True
			if city.country_id != country_id:
				models.write_log('City country is changed! Id: {}, old: {}, new: {}'.format(city.id, city.country_id, country_id), debug=debug)
				needs_update = True
			if needs_update:
				models.update_obj_if_needed(city, {'name': city_json['name'], 'region_id': region_id, 'country_id': country_id})
				n_updated += 1

	if cities_ids: # We delete cities only if some cities were found now
		for city in models.City.objects.exclude(pk__in=cities_ids).exclude(name='').order_by('pk'):
			print('City is deleted: {} (id {})'.format(city.name, city.id))
			models.update_obj_if_needed(city, {'name': '', 'region_id': None, 'country_id': None}, comment='Не встретили при обновлении')
			n_deleted += 1

	models.write_log('{} UPDATE CITIES finished. Created: {}, updated: {}, deleted: {}'.format(
		datetime.datetime.now(), n_created, n_updated, n_deleted), debug=debug)
	models.set_stat_value(name='n_cities', value=models.City.objects.count())

def get_teams_max_lengths(): # Returns 50 and 28
	max_name_length = 0
	max_city_length = 0
	for page in range(1, 35):# range(1, 35):
		page_json = comparing_util.url2json(models.Team.get_list_json_url(page=page))
		for team in page_json['items']:
			# print(team)
			max_name_length = max(max_name_length, len(team['name']))
			max_city_length = max(max_city_length, len(team['town']))
	print("Max name length:", max_name_length)
	print("Max city length:", max_city_length)

def get_tournaments_max_lengths(): # Returns 50 and 17
	max_name_length = 0
	max_type_length = 0
	for page in range(1, 6):# range(1, 35):
		page_json = comparing_util.url2json(models.Tournament.get_list_json_url(page=page))
		for tournament in page_json['items']:
			# print(tournament)
			max_name_length = max(max_name_length, len(tournament['name']))
			max_type_length = max(max_type_length, len(tournament['type_name']))
	print("Max name length:", max_name_length)
	print("Max type length:", max_type_length)

def save_tt_questions_taken(tt, results_string, questions_taken, questions_dropped, is_first_team, debug):
	# Saves the question for Team_on_tournament tt and update arrays questions_taken, questions_dropped
	n_blocks = 1 + (tt.tournament.questions_total - 1) // models.N_QUESTIONS_IN_BLOCK
	for block_number in range(n_blocks):
		n_questions_in_block = min((block_number + 1) * models.N_QUESTIONS_IN_BLOCK, tt.tournament.questions_total) \
			- block_number * models.N_QUESTIONS_IN_BLOCK
		res = 0
		for i in range(block_number * models.N_QUESTIONS_IN_BLOCK, block_number * models.N_QUESTIONS_IN_BLOCK + n_questions_in_block):
			res <<= 1
			if results_string[i] == '1':
				if questions_dropped[i]:
					print('Tournament {} (id {}) team {} (id {}), question {} is not dropped!'.format(
						tt.tournament.name, tt.tournament.id, tt.team.name, tt.team.id, i))
				questions_taken[i] += 1
				res += 1
			elif results_string[i] == 'X':
				if (not questions_dropped[i]) and (not is_first_team):
					print('Tournament {} (id {}), team {} (id {}), question {} is dropped!'.format(
						tt.tournament.name, tt.tournament.id, tt.team.name, tt.team.id, i))
				questions_dropped[i] = True
			elif results_string[i] not in '0?':
				print('Tournament {} (id {}) team {} (id {}), question {}: strange symbol "{}"'.format(
					tt.tournament.name, tt.tournament.id, tt.team.name, tt.team.id, i, results_string[i]))
		qt = models.Questions_taken.objects.create(
			team_on_tournament=tt,
			bits=res,
			block_number=block_number,
			n_questions=n_questions_in_block,
		)

def load_tt_players(tt, players_json, existing_player_ids, players_with_problems, debug=False):
	n_players_created = 0

	for player_json in players_json:
		person_json = player_json['player']
		player_id = person_json['id']
		if player_id not in existing_player_ids:
			player = models.Player.create_new(person_json, debug=debug)
			if player:
				existing_player_ids.add(player_id)
				n_players_created += 1
			else:
				models.write_log("Tournament {}, team {}: player with id {} does not exist".format(
					tt.tournament_id, tt.team_id, player_id), debug=debug)
				players_with_problems.add(player_id)
				continue

		if debug >= 2:
			print("Inserting: player {}, tournament {}, team {}, t_t {}".format(player_id, tt.tournament_id, tt.team_id, tt.id))
		models.Player_on_tournament.objects.create(
			team_on_tournament=tt,
			player_id=player_id,
			is_in_base_team=(player_json.get('flag') in ('К', 'Б')),
		)
	return n_players_created

# Also updates the list of teams in the base if new teams are found
def update_tournament_and_results(tournament, debug=False, comment='', all_teams: Optional[Dict[id, models.Team]]=None) -> Dict[id, models.Team]:
	existing_teams = tournament.team_on_tournament_set
	n_teams_with_wrong_mask_length = 0
	if existing_teams.exists():
		if debug:
			print('Tournament {}: Deleting {} old teams data'.format(tournament.id, existing_teams.count()))
		existing_teams.all().delete()
	url = tournament.get_results_participants_url()
	try:
		tournament_json = comparing_util.url2json(url)
	except:
		models.write_log("Tournament {}: Could not read data from {}. No results are loaded".format(tournament.id, url), debug=debug)
		return all_teams
	n_teams_loaded = 0
	n_teams_created = 0
	n_teams_with_results = 0
	n_venues_created = 0
	n_players_created = 0
	existing_player_ids = set(models.Player.objects.exclude(surname='').order_by('pk').values_list('pk', flat=True))

	players_with_problems = set()
	teams_with_problems = set()

	if tournament.questions_total:
		questions_taken = [0] * tournament.questions_total
		questions_dropped = [False] * tournament.questions_total
	if debug:
		print(f'Tournament {tournament.id}: Processing {len(tournament_json)} results')
	for team_json in tournament_json:
		team_id = team_json['team']['id']
		if all_teams and (team_id not in all_teams):
			team = models.Team.create_new(models.Team.get_dict_from_json(team_json['team']), comment=f'При загрузке результатов турнира {tournament.id}')
			if team:
				all_teams[team_id] = team
				n_teams_created += 1
				if debug:
					print('Team was created:', team.id)
			else:
				teams_with_problems.add(team_id)
				if debug:
					print(f'We have no teams with id {team_id}! Skipping')
				continue

		venue = None
		synch_request_id = None
		synch_request_json = team_json['synchRequest']
		if synch_request_json:
			synch_request_id = synch_request_json['id']
			venue_id = synch_request_json['venue']['id']
			venue, created = models.Venue.objects.get_or_create(pk=venue_id, defaults={'name': synch_request_json['venue']['name']})
			if created:
				n_venues_created += 1

		rating_json = team_json.get('rating')
		if rating_json is None:
			rating_json = {}

		if debug >= 2:
			print(f'Creating a result: tournament {tournament.id}, team {team_id}')
		tt = models.Team_on_tournament.objects.create(
			tournament=tournament,
			team_id=team_id,
			venue=venue,
			synch_request_id=synch_request_id,
			current_name=team_json['current']['name'],
			base_name=team_json['team']['name'],
			position=Decimal(team_json['position']) if team_json.get('position') else None,
			n_questions_taken=team_json.get('questionsTotal'),
			tech_rating_rb=rating_json.get('rb'),
			tech_rating_rg=rating_json.get('rg'),
			tech_rating_rt=rating_json.get('rt'),
			rating_r=rating_json.get('r'),
			predicted_position=rating_json.get('predictedPosition'),
			bonus_b=rating_json.get('b'),
			diff_bonus=rating_json.get('d'),
			is_included_in_rating=rating_json.get('inRating', False),
		)
		n_teams_loaded += 1
		n_players_created += load_tt_players(tt, team_json['teamMembers'], existing_player_ids, players_with_problems, debug=False)

		if not tournament.questions_total: # There is no info regarding the questions taken
			if debug >= 2:
				print(f'tournament.questions_total = {tournament.questions_total}. Continuing')
			continue
		results_string = team_json.get('mask')
		if not results_string:
			if debug >= 2:
				print(f'tournament {tournament.id}: no results_string')
			continue
		results_string = models.get_correct_mask(tournament, results_string) # TODO: check if can be deleted
		# if (not results_string) or (len(results_string) != tournament.questions_total):
		if len(results_string) != tournament.questions_total:
			n_teams_with_wrong_mask_length += 1
			if debug: # >= 2:
				print(f'Tournament {tournament.name} (id {tournament.id}) has {tournament.questions_total} questions, '
					+ f'but team {team_id} has only {len(results_string)}. Skipping')
			continue

		save_tt_questions_taken(tt, results_string, questions_taken, questions_dropped, n_teams_with_results == 0, debug=debug)
		n_teams_with_results += 1
	tournament.results_loaded_status = models.RESULTS_NOT_LOADED
	if n_teams_loaded > 0:
		tournament.results_loaded_status = models.RESULTS_ALL_LOADED if (n_teams_loaded == n_teams_with_results) else models.RESULTS_SOME_LOADED
	if n_teams_with_wrong_mask_length:
		if debug:
			print('Tournament {} (id {}) has {} teams with wrong mask length!'.format(
				tournament.name, tournament.id, n_teams_with_wrong_mask_length))
	tournament.n_teams_participated = n_teams_loaded
	tournament.n_teams_with_results = n_teams_with_results
	if tournament.questions_total:
		tournament.questions_dropped = sum(questions_dropped)
	tournament.save()

	tournament.question_data_set.all().delete()
	if n_teams_with_results:
		if debug > 1:
			print('Questions:')
		for i in range(tournament.questions_total):
			qd = models.Question_data.objects.create(
				tournament=tournament,
				number=i + 1,
				n_teams_taken=questions_taken[i],
				percent_teams_taken=int(round(questions_taken[i] * 100 / tournament.n_teams_with_results)),
				percent_teams_taken_rounded=int(round(questions_taken[i] * 100 / tournament.n_teams_with_results, -1)),
				is_dropped=questions_dropped[i],
			)
			if debug > 1:
				print('{}: {} or {}%'.format(i + 1, qd.n_teams_taken, qd.percent_teams_taken))

	if debug >= 2:
		print('Teams with correct data found:', n_teams_loaded)
		print('Teams created:', n_teams_created)
		print('Teams with results:', n_teams_with_results)
		print('Venues created:', n_venues_created)
		print('Players created:', n_players_created)

		print('Teams with problems:', len(teams_with_problems))
		print('Players with problems:', len(players_with_problems))

	if debug:
		if 0 < len(teams_with_problems) <= 100:
			print('Teams with problems:', ', '.join(str(i) for i in sorted(teams_with_problems)))
		if 0 < len(players_with_problems) <= 100:
			print('Players with problems:', ', '.join(str(i) for i in sorted(players_with_problems)))

	if debug > 1:
		print('Done with tournament {} (id {}):'.format(tournament.name, tournament.id))
		for field in models.Tournament._meta.get_fields():
			if not field.is_relation:
				print('{}: "{}"'.format(field.name, getattr(tournament, field.name)))
	return all_teams

def update_tournaments_completely(id_from: Optional[int]=0, date_end_from: Optional[datetime.date]=None, max_to_update: Optional[int]=None, debug: Optional[int]=0):
	models.write_log("{} UPDATE RECENT TOURNAMENTS started".format(datetime.datetime.now()), debug=debug)
	all_teams_in_base = {team.id: team for team in models.Team.objects.all()}
	n_updated = last_updated_id = 0
	today = datetime.date.today()
	if not date_end_from:
		date_end_from = today - datetime.timedelta(days=35)
	for tournament in list(models.Tournament.objects.filter(
			Q(is_archived=False) | Q(hide_detailed_results_until__gt=date_end_from),
			pk__gte=id_from,
			questions_total__gt=0,
			date_end__gt=date_end_from,
			date_end__lte=today).order_by('id')):
		all_teams_in_base = update_tournament_and_results(tournament, debug=debug, comment='Обновление недавних турниров', all_teams=all_teams_in_base)
		n_updated += 1
		last_updated_id = tournament.id
		if max_to_update and (n_updated >= max_to_update):
			break
	models.write_log(f'{datetime.datetime.now()} UPDATE RECENT TOURNAMENTS finished. Updated results of tournaments: {n_updated}. Last updated ID: {last_updated_id}',
		debug=debug)

def check_tournament_pages(id_from=0):
	n_updated = 0
	for tournament in list(models.Tournament.objects.filter(pk__gte=id_from).exclude(name='').order_by('pk')):
		url = tournament.get_results_participants_url()
		try:
			tournament_json = comparing_util.url2json(url)
		except:
			print("Tournament {}: Could not read data from {}. No results are loaded".format(tournament.id, url))
		n_updated += 1
		if (n_updated % 100) == 0:
			print(tournament.id)
	print("{} CHECK TOURNAMENTS finished. Updated results of tournaments: {}".format(datetime.datetime.now(), n_updated))

def update_players(page_from=1, debug=False, to_log_new_players=False):
	models.write_log("{} UPDATE PLAYERS started".format(datetime.datetime.now()), debug=debug)
	all_players_in_base = {player.id: player for player in models.Player.objects.all()}
	# all_teams_in_base = set(models.player.objects.all().values_list('id'))
	n_created = 0
	n_updated = 0
	n_deleted = 0
	n_pages_broken = 0

	page = page_from - 1
	while True:
		page += 1
		url = models.Player.get_list_json_url(page)
		try:
			page_json = comparing_util.url2json(url)
			if debug:
				print(f'{url} contains {len(page_json)} player records')
		except:
			models.write_log(f'Players page is broken: {url}', debug=debug)
			n_pages_broken += 1
			break
		if len(page_json) == 0:
			break
		for player_json in page_json:
			player_id = player_json['id']
			if player_id in all_players_in_base:
				player = all_players_in_base.pop(player_id)
				if player.update_if_needed(player_json):
					n_updated += 1
			else:
				# TODO: Delete when https://gl.appris.by/rating_mak/rating_public/-/issues/1123 is fixed
				if player_json['patronymic'] is None:
					player_json['patronymic'] = ''
				models.Player.create_new(player_json, to_log=to_log_new_players)
				n_created += 1
		if debug:
			print('Page', page, 'is processed')
	if page_from == 1 and n_pages_broken == 0:
		for player_id in all_players_in_base:
			player = models.Player.objects.get(pk=player_id)
			if player.update_if_needed({'surname': '', 'name': '', 'patronymic': ''}):
				n_deleted += 1
	models.write_log("{} UPDATE PLAYERS finished. Broken pages: {}. New players: {}, updated players: {}, deleted players: {}".format(
		datetime.datetime.now(), n_pages_broken, n_created, n_updated, n_deleted), debug=debug)
	models.set_stat_value(name='n_players', value=models.Player.objects.exclude(surname='', name='', patronymic='').count())

# def load_players_on_tournaments(player_from=1, player_to=10000000, debug=False): # Usually isn't used
# 	models.write_log("{} UPDATE PLAYERS_ON_TOURNAMENTS started".format(datetime.datetime.now()), debug=debug)
# 	n_deleted, _ = models.Player_on_tournament.objects.filter(player_id__gte=player_from, player_id__lte=player_to).delete()
# 	if debug:
# 		print('Deleted from Player_on_tournament:', n_deleted)
# 	player_ids = models.Player.objects.filter(pk__gte=player_from, pk__lte=player_to).order_by('pk').values_list('pk', flat=True)
# 	team_ids = set(models.Team.objects.exclude(name='').order_by('pk').values_list('pk', flat=True))
# 	tournament_ids = set(models.Tournament.objects.exclude(name='').order_by('pk').values_list('pk', flat=True))
# 	# all_teams_in_base = set(models.player.objects.all().values_list('id'))
# 	n_created = 0
# 	n_players_processed = 0
# 	n_pages_broken = 0
# 	players_with_problems = set()
# 	n_teams_created = 0
# 	n_tournaments_created = 0
# 	n_tournaments_updated = 0

# 	for player_id in player_ids:
# 		url = models.Player.get_tournaments_list_url(player_id)
# 		try:
# 			page_json = comparing_util.url2json(url)
# 		except:
# 			models.write_log("Player page is broken: {}".format(url), debug=debug)
# 			n_pages_broken += 1
# 			players_with_problems.add(player_id)
# 			continue
# 		if isinstance(page_json, dict):
# 			for season, data in page_json.items():
# 				for t_json in data['tournaments']:
# 					team_id = models.int_safe(t_json['idteam'])
# 					if team_id == 0:
# 						models.write_log("Player page {}, season {}: invalid team id {}".format(url, season, t_json['idteam']), debug=debug)
# 						players_with_problems.add(player_id)
# 						continue
# 					if team_id not in team_ids:
# 						team = try_create_team(team_id, debug=debug)
# 						if team:
# 							team_ids.add(team_id)
# 							n_teams_created += 1
# 						else:
# 							models.write_log("Player page {}, season {}: team id {} does not exist".format(url, season, team_id), debug=debug)
# 							players_with_problems.add(player_id)
# 							continue

# 					tournament_id = models.int_safe(t_json['idtournament'])
# 					if tournament_id == 0:
# 						models.write_log("Player page {}, season {}: invalid tournament id {}".format(url, season, t_json['idtournament']),
# 							debug=debug)
# 						players_with_problems.add(player_id)
# 						continue
# 					if tournament_id not in tournament_ids:
# 						tournament = models.Tournament.create_new(tournament_id, debug=debug, comment='Обнаружили новый турнир при обновлении')
# 						if tournament:
# 							tournament_ids.add(tournament_id)
# 							n_tournaments_created += 1
# 						else:
# 							models.write_log("Player page {}, season {}: tournament id {} does not exist".format(url, season, tournament_id),
# 								debug=debug)
# 							players_with_problems.add(player_id)
# 							continue

# 					t_t = models.Team_on_tournament.objects.filter(team_id=team_id, tournament_id=tournament_id).first()
# 					if not t_t:
# 						update_tournament_and_results(models.Tournament.objects.get(pk=tournament_id), debug=debug)
# 						n_tournaments_updated += 1
# 						t_t = models.Team_on_tournament.objects.filter(team_id=team_id, tournament_id=tournament_id).first()
# 						if not t_t:
# 							models.write_log("Player page {}, season {}: team {} did not play at tournament {}".format(
# 								url, season, team_id, tournament_id), debug=debug)
# 							players_with_problems.add(player_id)
# 							continue

# 					if debug >= 2:
# 						print("Inserting: player {}, team {}, tournament {}, t_t {}".format(player_id, team_id, tournament_id, t_t.id))
# 					models.Player_on_tournament.objects.create(
# 						team_on_tournament=t_t,
# 						player_id=player_id,
# 						is_in_base_team=(t_json['in_base_team'] == '1')
# 					)
# 					n_created += 1
# 		n_players_processed += 1
# 		if (n_players_processed % 1000) == 0:
# 			if debug:
# 				print('Player', player_id, 'is processed')
# 			time.sleep(2)
# 	models.write_log(("{} UPDATE PLAYERS_ON_TOURNAMENTS finished. Broken pages: {}. New rows: {}, deleted rows: {}, players processed: {}, "
# 		+ "players with problems: {}").format(
# 		datetime.datetime.now(), n_pages_broken, n_created, n_deleted, n_players_processed, len(players_with_problems)), debug=debug)
# 	models.write_log("Teams created: {}, tournaments created: {}, tournaments updated: {}".format(
# 		n_teams_created, n_tournaments_created, n_tournaments_updated), debug=debug)
# 	if 0 < len(players_with_problems) <= 100:
# 		models.write_log("Players with problems: {}".format(', '.join(str(i) for i in sorted(players_with_problems))), debug=debug)
# 	models.set_stat_value(name='n_players_on_tournaments', value=models.Player_on_tournament.objects.count())

def latest_rating_url(page=1):
	return f'{models.MAII_API_URL}teams/latest.json?page={page}'

def update_teams_rating(debug=0):
	models.write_log(f'{datetime.datetime.now()} UPDATE TEAMS RATING started', debug=debug)
	all_teams_in_base = set(models.Team.objects.all().values_list('pk', flat=True))
	page = 0
	n_ratings_loaded = n_unknown_teams_met = 0
	models.Team.objects.all().update()
	while True:
		page += 1
		url = latest_rating_url(page=page)
		try:
			rating_json = comparing_util.url2json(url)
		except:
			models.write_log(f'Could not read data from {url}. No ratings are loaded', debug=debug)
			return
		if debug:
			print(f'Processing page {page}...')
		if len(rating_json['items']) == 0:
			break
		for item in rating_json['items']:
			team_id = item['team_id']
			if team_id in all_teams_in_base:
				models.Team.objects.filter(pk=team_id).update(rating_score=item['rating'], rating_place=(item['top_place'] + item['bottom_place']) // 2)
				n_ratings_loaded += 1
			else:
				n_unknown_teams_met += 1
	models.set_stat_value(name='ratings_loaded', value=n_ratings_loaded)
	models.write_log(
		f'{datetime.datetime.now()} UPDATE TEAMS RATING finished. ratings loaded: {n_ratings_loaded}, pages processed: {page-1}, unknown teams met: {n_unknown_teams_met}',
		debug=debug)
