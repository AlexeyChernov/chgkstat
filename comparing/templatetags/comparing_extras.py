# -*- coding: utf-8 -*-
from django import template

from comparing.comparing_util import plural_ending

register = template.Library()

@register.filter
def ending(value, word_type):
	return plural_ending(value, word_type)
