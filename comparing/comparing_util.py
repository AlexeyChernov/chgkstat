import datetime
import urllib.request
import json

ITEMS_PER_PAGE = 1000

plural_endings_array = [
	(), # 0
	('', 'а', 'ов'), # 1 # For e.g. 'результат'
	('а', 'ы', ''),  # 2 # For e.g. 'трасса', 'команда','женщина'
	('ь', 'и', 'ей'), # 3 # For e.g. 'новость'
	('я', 'и', 'й'), # 4 # For e.g. 'дистанция'
	('й', 'х', 'х'), # 5 # For e.g. 'завершивши_ся', 'предстоящий', 'похожий'
	('й', 'е', 'е'), # 6 # For e.g. 'который'
	('е', 'я', 'й'), # 7 # For e.g. 'предупреждение'
	('ка', 'ки', 'ок'), # 8 # For e.g. 'ошибка'
	('', 'а', ''),   # 9 # For e.g. 'человек'
	(), # 10 # For e.g. 'планиру?т'
	('год', 'года', 'лет'), # 11 # For e.g. 'год/лет'
	('', 'и', 'и'), # 12 # For e.g. 'ваш'
	('а', '', ''),   # 13 # For e.g. 'мужчина'
	('и', 'ях', 'ях'), # 14 # For e.g. 'о дистанциях'
	('е', 'ах', 'ах'), # 15 # For e.g. 'о забегах', 'на турнирах'
	('а', 'ов', 'ов'), # 16 # For e.g. 'с трёх забегов'
	('я', 'и', 'их'), # 17 # For e.g. 'ничья'
	('ё', 'у', 'у'), # 18 # For e.g. 'ид_т'
	('у', 'ы', ''), # 19 # For e.g. 'игр'
	('ом', 'ых', 'ых'), # 20 # For e.g. 'рейтинговом'
	('а', 'и', 'и'), # 21 # For e.g. 'сыграла'
	('ая', 'ие', 'их'), # 22 # For e.g. 'другая'
]

def plural_ending(value, word_type):
	if value is None:
		value = 0
	value %= 100
	if word_type == 10:
		return 'е' if (value == 1) else 'ю'
	endings = plural_endings_array[word_type]
	if 11 <= value <= 19:
		return endings[2]
	value %= 10
	if value == 0:
		return endings[2]
	if value == 1:
		return endings[0]
	if value >= 5:
		return endings[2]
	return endings[1]

def url2json(url):
	response = urllib.request.urlopen(url)
	return json.load(response)

def print_semiwhole_number(n):
	if n is None:
		return ''
	if n % 1 == 0:
		return '{}<span class="invisible">.5</span>'.format(int(n))
	return n
