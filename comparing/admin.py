from django.contrib import admin

from .models import Team, Tournament, Table_update, Field_update

admin.site.register(Team)
admin.site.register(Tournament)
admin.site.register(Table_update)
admin.site.register(Field_update)
