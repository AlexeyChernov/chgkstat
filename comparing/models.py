from django.db.models.signals import pre_delete
from django.utils.safestring import mark_safe
from django.urls import reverse
from django.dispatch import receiver
from django.db.models import Max
from django.db import models
import datetime
import io
from pathlib import Path

from . import comparing_util

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent

SITE_URL = 'http://chgkstat.org'
RATING_SITE_URL = 'https://rating.chgk.info/'
API_SITE_URL = RATING_SITE_URL + 'api/'
NEW_API_SITE_URL = 'http://api.rating.chgk.net/'
# API_SITE_URL = 'https://prestable.rating.chgk.net/api/'
MAII_API_URL = 'https://rating.maii.li/api/v1/b/'
TOUCH_TO_RELOAD_PATH = BASE_DIR / 'touch_reload'
STRANGE_CITIES = ('-', 'сборная')

def int_safe(s, default=0):
	try:
		res = int(s)
	except:
		res = default
	return res

LOG_FILE = BASE_DIR / '.logs' / 'chgkproj_django.log'
def write_log(s, debug=False):
	with io.open(LOG_FILE, "a", encoding="utf8") as f:
	#	try:
			f.write(s + "\n")
	#	except:
	#		f.write((s + "\n").encode('utf8'))
	if debug:
		print(s)

months = [''] * 13
months[1] = 'января'
months[2] = 'февраля'
months[3] = 'марта'
months[4] = 'апреля'
months[5] = 'мая'
months[6] = 'июня'
months[7] = 'июля'
months[8] = 'августа'
months[9] = 'сентября'
months[10] = 'октября'
months[11] = 'ноября'
months[12] = 'декабря'
def date2str(mydate, with_nbsp=True, short_format=False):
	if short_format:
		return mydate.strftime('%d.%m.%Y')
	else:
		return u"{}{}{} {}".format(mydate.day, '&nbsp;' if with_nbsp else ' ', months[mydate.month], mydate.year)
def dates2str(start_date, finish_date):
	if (start_date is None) or (finish_date is None):
		return 'даты неизвестны'
	if start_date == finish_date:
		return date2str(start_date)
	if start_date.year == finish_date.year:
		if start_date.month == finish_date.month:
			return u"{}–{}&nbsp;{} {}".format(
				start_date.day, finish_date.day, months[start_date.month], start_date.year)
		else:
			return u"{}&nbsp;{}&nbsp;– {}&nbsp;{} {}".format(
				start_date.day, months[start_date.month], finish_date.day, months[finish_date.month], start_date.year)
	else:
		return u"{}&nbsp;– {}".format(
				date2str(start_date), date2str(finish_date))

def str2date(s):
	return datetime.datetime.strptime(s, "%Y-%m-%d %H:%M:%S").date()

def log_obj_create(obj, comment=''):
	model = obj.__class__
	table_update = Table_update.objects.create(model_name=model.__name__, row_id=obj.id, action_type=ACTION_CREATE, comment=comment)
	for field in model._meta.get_fields():
		if not field.is_relation:
			if getattr(obj, field.name):
				Field_update.objects.create(table_update=table_update, field_name=field.name,
					new_value=str(getattr(obj, field.name))[:TEXT_FIELD_LENGTH])

def update_obj_if_needed(obj, t_json, comment=''):
	needs_to_save = False
	table_update = None
	for field in obj.__class__._meta.get_fields():
		for field_name in (field.name, field.name + '_id'):
			if field_name in t_json:
				json_value = t_json[field_name]
				if (json_value is None) and (field.__class__.__name__ == 'CharField'):
					json_value = ''
				if getattr(obj, field_name) != json_value:
					if field_name != 'is_in_rating': # It's not interesting to monitor
					# if (field_name != 'is_in_rating') and (field_name != 'city'): # It's not interesting to monitor
						if table_update is None:
							table_update = Table_update.objects.create(
								model_name=obj.__class__.__name__,
								row_id=obj.id,
								action_type=ACTION_UPDATE,
								comment=comment # 'Заметили, что объект изменился'
							)
						Field_update.objects.create(
							table_update=table_update,
							field_name=field_name,
							old_value=getattr(obj, field_name) if getattr(obj, field_name) else '',
							new_value=json_value if json_value else '',
						)
					setattr(obj, field_name, json_value)
					needs_to_save = True
	if hasattr(obj, 'city') and hasattr(obj, 'city_name') and obj.city_name and (obj.city is None) and (obj.city_name not in STRANGE_CITIES): # TODO: Fix to remove city_name
		obj.city = City.objects.filter(name=obj.city_name).first()
		if obj.city:
			needs_to_save = True
	if needs_to_save:
		obj.save()
	return needs_to_save

def print_obj_fields(obj):
	for field in obj.__class__._meta.get_fields():
		if not field.is_relation:
			print('{} = "{}"'.format(field.name, getattr(obj, field.name)))

def set_stat_value(name, value):
	stat, _ = Statistics.objects.get_or_create(name=name, date_added=datetime.date.today())
	stat.value = value
	stat.save()
	write_log('{}: {} = {}'.format(datetime.datetime.now(), name, value))

def get_stat_value(name):
	stat = Statistics.objects.filter(name=name).order_by('-date_added').first()
	return stat.value if stat else None

class Country(models.Model):
	# We don't make the name unique as we set name='' for deleted objects instead of deleting them from our DB.
	name = models.CharField(verbose_name='Название', max_length=100, db_index=True)
	@classmethod
	def get_list_json_url(cls):
		return '{}countries?pagination=false'.format(NEW_API_SITE_URL)
	def __str__(self):
		return self.name

class District(models.Model): # Федеральный округ
	country = models.ForeignKey(Country, verbose_name='Страна', on_delete=models.CASCADE)
	name = models.CharField(verbose_name='Название', max_length=100, db_index=True)
	def __str__(self):
		return self.name

class Region(models.Model):
	country = models.ForeignKey(Country, verbose_name='Страна', on_delete=models.CASCADE, blank=True, null=True)
	district = models.ForeignKey(District, verbose_name='Федеральный округ', on_delete=models.CASCADE, blank=True, null=True, default=None)
	name = models.CharField(verbose_name='Название', max_length=100, db_index=True)
	@classmethod
	def get_list_json_url(cls):
		return '{}regions?pagination=false'.format(NEW_API_SITE_URL)
	def __str__(self):
		return self.name

class City(models.Model):
	country = models.ForeignKey(Country, verbose_name='Страна', on_delete=models.CASCADE, blank=True, null=True)
	region = models.ForeignKey(Region, verbose_name='Регион', on_delete=models.CASCADE, blank=True, null=True)
	name = models.CharField(verbose_name='Название', max_length=100, db_index=True)
	@classmethod
	def get_list_json_url(cls):
		return '{}towns?pagination=false'.format(NEW_API_SITE_URL)
	def __str__(self):
		return self.name

class Venue(models.Model):
	name = models.CharField(verbose_name='Название', max_length=100, db_index=True)
	city = models.ForeignKey(City, verbose_name='Город', on_delete=models.SET_NULL, blank=True, null=True, default=None)
	@classmethod
	def get_list_json_url(cls):
		return '{}venues?pagination=false'.format(NEW_API_SITE_URL)
	def get_rating_url(self):
		return '{}venues.php?id={}'.format(RATING_SITE_URL, self.id)
	def __str__(self):
		return self.name

def get_team_cur_rating_json_url(team_id):
	return '{}teams/{}/rating/b.json'.format(API_SITE_URL, team_id)

def get_team_details_json_url(team_id):
	return f'{NEW_API_SITE_URL}teams/{team_id}'

TEXT_FIELD_LENGTH = 120
FIELD_NAME_LENGTH = 30
URL_MAX_LENGTH = 200
class Team(models.Model):
	name = models.CharField(verbose_name='Название', max_length=TEXT_FIELD_LENGTH, blank=True, db_index=True)
	city = models.ForeignKey(City, verbose_name='Город', on_delete=models.SET_NULL, blank=True, null=True, default=None)
	city_name = models.CharField(verbose_name='Название города', max_length=TEXT_FIELD_LENGTH, blank=True, db_index=True) # TODO: Delete
	added_time = models.DateTimeField(verbose_name='Время занесения в БД', auto_now_add=True)
	rating_score = models.IntegerField(verbose_name='Очки рейтинга', default=None, null=True, blank=True)
	rating_place = models.IntegerField(verbose_name='Место в рейтинге', default=None, null=True, blank=True)
	n_games_last_year = models.IntegerField(verbose_name='Сколько рейтинговых игр сыграно за последний год', db_index=True,
		default=None, null=True, blank=True)
	n_teams_played_many_games_with_me = models.IntegerField(verbose_name='Сколько команд сыграли достаточно игр со мной для Team_close_to_mine за последний год',
		default=None, null=True, blank=True)
	def update_if_needed(self, team_json, comment):
		return update_obj_if_needed(self, team_json, comment)
	def get_json_for_select(self):
		return {'id': self.id, 'text': str(self)}
	def get_reverse_url(self, target):
		return reverse(target, kwargs={'team_id': self.id})
	def get_absolute_url(self):
		return self.get_reverse_url('team_details')
	def get_rating_url(self):
		return '{}teams/{}'.format(RATING_SITE_URL, self.id)
	def get_results_json_url(self, tournament):
		return '{}tournaments/{}/results/{}.json'.format(API_SITE_URL, tournament.id, self.id)
	def get_cur_rating_json_url(self):
		return get_team_cur_rating_json_url(self.id)
	def pr(self):
		print_obj_fields(self)
	def __str__(self):
		return '{} ({}, id {})'.format(self.name, self.city.name, self.id)
	@classmethod
	def get_dict_from_json(cls, json):
		res = {}
		res['id'] = json['id']
		res['name'] = json['name']
		res['city_id'] = None
		if 'town' in json:
			res['city_id'] = json['town']['id']
		return res
	@classmethod
	def create_new(cls, json, comment=''):
		team = cls()
		for key, val in json.items():
			setattr(team, key, val)
		team.save()
		log_obj_create(team, comment=comment)
		return team
	@classmethod
	def get_list_json_url(cls, page=0):
		return f'{NEW_API_SITE_URL}teams?page={page}&itemsPerPage=1000&pagination=true'
@receiver(pre_delete, sender=Team)
def pre_team_delete(sender, instance, **kwargs):
	Table_update.objects.filter(model_name=instance.__class__.__name__, row_id=instance.id).delete()

def get_player_details_json_url(player_id):
	return '{}players/{}.json'.format(API_SITE_URL, player_id)

class Player(models.Model):
	surname = models.CharField(verbose_name='Фамилия', max_length=TEXT_FIELD_LENGTH, blank=True)
	name = models.CharField(verbose_name='Имя', max_length=TEXT_FIELD_LENGTH, blank=True)
	patronymic = models.CharField(verbose_name='Отчество', max_length=TEXT_FIELD_LENGTH, blank=True)
	n_games = models.SmallIntegerField(verbose_name='Число игр', default=None, null=True, blank=True)
	n_rating_games = models.SmallIntegerField(verbose_name='Число игр, учтенных в рейтинге', default=None, null=True, blank=True)
	n_partners = models.SmallIntegerField(verbose_name='Число партнеров по столу', default=None, null=True, blank=True)
	added_time = models.DateTimeField(verbose_name='Время занесения в БД', auto_now_add=True)
	class Meta:
		index_together = [["surname", "name", "patronymic"],]
	def update_if_needed(self, player_json, comment=''):
		return update_obj_if_needed(self, player_json, comment)
	def get_rating_url(self):
		return '{}player/{}'.format(RATING_SITE_URL, self.id)
	def get_tournaments_list_url(self):
		return self.__class__.get_tournaments_list_url(self.id)
	def pr(self):
		print_obj_fields(self)
	def get_name(self):
		return '{} {} {}'.format(self.surname, self.name, self.patronymic)
	def __str__(self):
		return '{} {} {} (id {})'.format(self.surname, self.name, self.patronymic, self.id)
	@classmethod
	def create_new(cls, json, debug=False, to_log=False, comment=''):
		player = cls()
		for key, val in json.items():
			setattr(player, key, val)
		player.save()
		if to_log:
			log_obj_create(player, comment=comment)
		return player
	@classmethod
	def get_list_json_url(cls, page=1):
		return f'{NEW_API_SITE_URL}players?page={page}&itemsPerPage=5000'
	@classmethod
	def get_tournaments_list_url(cls, player_id):
		return f'{NEW_API_SITE_URL}players/{player_id}/tournaments?pagination=false'

RESULTS_NOT_LOADED = 0
RESULTS_SOME_LOADED = 1
RESULTS_ALL_LOADED = 2
RESULTS_LOAD_STATUSES = (
	(RESULTS_NOT_LOADED, 'Результаты не загружены'),
	(RESULTS_SOME_LOADED, 'Загружены результаты части команд'),
	(RESULTS_ALL_LOADED, 'Загружены результаты всех команд'),
)

TOURNAMENT_TYPES = (
	(1, 'Неизвестный'),
	(2, 'Обычный'),
	(3, 'Синхрон'),
	(4, 'Региональный'),
	(5, 'Общий зачёт'),
	(6, 'Строго синхронный'),
	(8, 'Асинхрон'),
	(10, 'Марафон'),
	(11, 'Онлайн'),
)
IN_RATING_TYPES = (2, 3, 4, 6)
IN_RATING_OFFLINE_TYPES = (2, 4)
IN_RATING_SYNCHRON_TYPES = (3, 6)
class Tournament(models.Model):
	name = models.CharField(verbose_name='Название', max_length=TEXT_FIELD_LENGTH, blank=True, db_index=True)
	city = models.ForeignKey(City, verbose_name='Город', on_delete=models.SET_NULL, blank=True, null=True, default=None)
	city_name = models.CharField(verbose_name='Город', max_length=TEXT_FIELD_LENGTH, blank=True, db_index=True) # TODO: Delete
	t_type = models.SmallIntegerField(verbose_name='Тип турнира', choices=TOURNAMENT_TYPES, default=1)
	type_name = models.CharField(verbose_name='Тип', max_length=TEXT_FIELD_LENGTH, blank=True, db_index=True)
	date_start = models.DateField(verbose_name='Дата начала', default=None, null=True, blank=True, db_index=True)
	date_end = models.DateField(verbose_name='Дата окончания', default=None, null=True, blank=True, db_index=True)
	hide_results_until = models.DateField(verbose_name='Скрывать число взятых до', default=None, null=True, blank=True, db_index=True)
	hide_detailed_results_until = models.DateField(verbose_name='Скрывать повопросную таблицу до', default=None, null=True, blank=True, db_index=True)
	added_time = models.DateTimeField(verbose_name='Время занесения в БД', auto_now_add=True)
	questions_per_tour = models.SmallIntegerField(verbose_name='Число вопросов в туре', default=None, null=True, blank=True)
	questions_per_tour_details = models.CharField(verbose_name='Число вопросов в туре — подробно', max_length=200)
	tour_count = models.SmallIntegerField(verbose_name='Число туров', default=None, null=True, blank=True)
	questions_total = models.SmallIntegerField(verbose_name='Число вопросов всего', default=None, null=True, blank=True)
	questions_dropped = models.SmallIntegerField(verbose_name='Число снятых по апелляциям вопросов', default=None, null=True, blank=True)

	results_loaded_status = models.SmallIntegerField(verbose_name='Состояние результатов', choices=RESULTS_LOAD_STATUSES,
		default=RESULTS_NOT_LOADED, db_index=True)
	is_included_in_rating = models.BooleanField(verbose_name='Учтен ли в рейтинге результат хоть одной команды',
		default=False, blank=True, db_index=True)
	is_in_rating = models.BooleanField(verbose_name='Учтен ли в рейтинге', default=False, blank=True, db_index=True)
	is_archived = models.BooleanField(verbose_name='Убрал ни в архив', default=False, blank=True, db_index=True)
	n_teams_participated = models.SmallIntegerField(verbose_name='Число команд-участниц', default=None, null=True, blank=True, db_index=True)
	n_teams_with_results = models.SmallIntegerField(verbose_name='Число команд с полными результатами',
		default=None, null=True, blank=True, db_index=True)

	site_url = models.CharField(verbose_name='Сайт турнира', max_length=URL_MAX_LENGTH, blank=True)
	lj_link = models.CharField(verbose_name='Обсуждение вопросов в ЖЖ', max_length=URL_MAX_LENGTH, blank=True)
	kand_link = models.CharField(verbose_name='Оценки вопросов в Канделябре', max_length=URL_MAX_LENGTH, blank=True)
	def update_if_needed(self, t_json, comment=''):
		return update_obj_if_needed(self, t_json, comment)
	def update_from_api_if_needed(self, comment=''):
		tournament_data = self.get_dict_from_api()
		return self.update_if_needed(tournament_data, comment), tournament_data
	def get_json_for_select(self):
		return {'id': self.id, 'text': str(self).replace('&nbsp;', ' ')}
	def get_reverse_url(self, target):
		return reverse(target, kwargs={'tournament_id': self.id})
	def get_absolute_url(self):
		return self.get_reverse_url('all_teams_on_tournament')
	def get_compare_teams_url(self):
		return self.get_reverse_url('compare_teams_list_on_tournament')
	def get_questions_table_url(self):
		return self.get_reverse_url('tournament_questions_table')
	def get_update_url(self):
		return self.get_reverse_url('update_tournament')
	def get_edit_questions_url(self):
		return self.get_reverse_url('edit_tournament_questions')
	def get_rating_url(self):
		return '{}tournament/{}'.format(RATING_SITE_URL, self.id)
	def get_info_json_url(self):
		return '{}tournaments/{}.json'.format(API_SITE_URL, self.id)
	def get_all_teams_url(self):
		return f'{NEW_API_SITE_URL}tournaments/{self.id}/results?includeMasksAndControversials=1'
	def get_participants_url(self):
		return '{}tournaments/{}/recaps.json'.format(API_SITE_URL, self.id)
	def get_results_participants_url(self):
		return f'{NEW_API_SITE_URL}tournaments/{self.id}/results?includeTeamMembers=1&includeMasksAndControversials=1&includeTeamFlags=0&includeRatingB=1'
	def get_dates(self):
		return dates2str(self.date_start, self.date_end)
	def get_tour_lengths(self):
		if self.questions_per_tour_details:
			return [int(x) for x in self.questions_per_tour_details.replace(' ', '').replace(',', ';').split(';')]
		if self.questions_per_tour and self.tour_count:
			return [self.questions_per_tour] * self.tour_count
		return None
	@classmethod
	def get_dict_from_json(cls, json):
		res = {}
		res['id'] = json['id']
		res['name'] = json['name']
		res['city_id'] = json.get('idtown')
		res['date_start'] = datetime.datetime.fromisoformat(json['dateStart']).date()
		res['date_end'] = datetime.datetime.fromisoformat(json['dateEnd']).date()
		res['t_type'] = json['type']['id']
		res['is_archived'] = False
		if 'hideResultsTo' in json:
			res['hide_results_until'] = datetime.datetime.fromisoformat(json['hideResultsTo'])
		if 'hideQuestionsTo' in json:
			res['hide_detailed_results_until'] = datetime.datetime.fromisoformat(json['hideQuestionsTo'])
		if 'synchData' in json:
			res['is_archived'] = json['synchData'].get('archive', False)
		res['is_in_rating'] = json.get('tournamentInRatingBalanced', False) or json.get('maiiRating', False)

		tours_dict = json.get('questionQty')
		if tours_dict:
			tour_lengths = [v for k, v in sorted(tours_dict.items())]
			res['tour_count'] = len(tour_lengths)
			res['questions_total'] = sum(tour_lengths)
			if len(set(tour_lengths)) == 1:
				res['questions_per_tour'] = tour_lengths[0]
			else:
				res['questions_per_tour_details'] = ','.join(str(i) for i in tour_lengths)
		if not res:
			write_log("{} Problem with loading data for tournament with id {}".format(datetime.datetime.now(), self.id), debug=True)
		return res
	def check_tour_lengths(self):
		if not self.questions_total:
			return None, False, 'У турнира {} (id {}) не указано число вопросов.'.format(tournament.name, tournament.id)
		tour_lengths = self.get_tour_lengths()
		if not tour_lengths:
			return tour_lengths, False, 'У турнира {} (id {}) не указаны размеры туров.'.format(tournament.name, tournament.id)
		if sum(tour_lengths) != self.questions_total:
			return (tour_lengths, False,
				'У турнира {} (id {}) что-то не так с размерами туров: в них должно быть {} вопросов, но всего должно быть {} вопросов.'.format(
					self.name, self.id, ', '.join(str(i) for i in tour_lengths), self.questions_total)
			)
		return tour_lengths, True, ''
	def pr(self):
		print_obj_fields(self)
	def __str__(self):
		return '{} ({}, id {})'.format(self.name, self.get_dates(), self.id)
	@classmethod
	def create_new(cls, json, debug=False, comment=''):
		tournament = cls()
		# try:
		for key, val in json.items():
			setattr(tournament, key, val)
		tournament.save()
		log_obj_create(tournament, comment=comment)
		return tournament
		# except Exception as e:
		# 	write_log("{} Could not load data of exact tournament from URL {}: {}".format(
		# 		datetime.datetime.now(), tournament.get_info_json_url(), e), debug=debug)
		# 	return None
	@classmethod
	def get_list_json_url(cls, page=1):
		return f'{NEW_API_SITE_URL}tournaments.json?page={page}&itemsPerPage=500'
@receiver(pre_delete, sender=Tournament)
def pre_tournament_delete(sender, instance, **kwargs):
	Table_update.objects.filter(model_name=instance.__class__.__name__, row_id=instance.id).delete()

ACTION_ANY = 0
ACTION_CREATE = 1
ACTION_UPDATE = 2
ACTION_DELETE = 3
ACTION_TYPES = (
	(ACTION_ANY, 'Любое действие'),
	(ACTION_CREATE, 'Создание'),
	(ACTION_UPDATE, 'Изменение'),
	(ACTION_DELETE, 'Удаление')
)
class Table_update(models.Model):
	model_name = models.CharField(verbose_name='Название модели таблицы', max_length=FIELD_NAME_LENGTH)
	row_id = models.IntegerField(verbose_name='id строки в таблице', default=0, db_index=True)
	action_type = models.SmallIntegerField(verbose_name='Действие', choices=ACTION_TYPES, db_index=True)
	comment = models.CharField(verbose_name='Комментарий', max_length=100)
	added_time = models.DateTimeField(verbose_name='Время занесения в БД', auto_now_add=True)
	def print_field_updates(self):
		print(self)
		for field_name in self.field_update_set.all():
			print(field_name)
	def __str__(self):
		return '{} объекта "{}" (id {})'.format(self.get_action_type_display(), self.model_name, self.row_id)

class Field_update(models.Model):
	table_update = models.ForeignKey(Table_update, verbose_name='Обновление таблицы', on_delete=models.CASCADE)
	field_name = models.CharField(verbose_name='Название изменённого поля', max_length=FIELD_NAME_LENGTH)
	old_value = models.CharField(verbose_name='Старое значение', max_length=TEXT_FIELD_LENGTH, blank=True)
	new_value = models.CharField(verbose_name='Новое значение', max_length=TEXT_FIELD_LENGTH, blank=True)
	def __str__(self):
		return 'Поле: {}. "{}" -> "{}"'.format(self.field_name, self.old_value, self.new_value)

class Question_text(models.Model):
	tournament = models.ForeignKey(Tournament, verbose_name='Турнир', on_delete=models.CASCADE)
	number = models.SmallIntegerField(verbose_name='Номер вопроса')
	text = models.TextField(verbose_name='Текст вопроса', blank=True)
	answer = models.TextField(verbose_name='Ответ', blank=True)
	criteria = models.TextField(verbose_name='Зачёт', blank=True)
	author = models.TextField(verbose_name='Автор', blank=True)
	comment = models.TextField(verbose_name='Комментарий', blank=True)
	source = models.TextField(verbose_name='Источники', blank=True)

	lj_link = models.CharField(verbose_name='Обсуждение в ЖЖ', max_length=URL_MAX_LENGTH, blank=True)

	added_time = models.DateTimeField(verbose_name='Время занесения в БД', auto_now_add=True)
	def __str__(self):
		return 'Вопрос № {} на турнире '.format(self.number) + str(self.tournament)
	class Meta:
		unique_together = (("tournament", "number"),)

class Question_data(models.Model):
	tournament = models.ForeignKey(Tournament, verbose_name='Турнир', on_delete=models.CASCADE)
	number = models.SmallIntegerField(verbose_name='Номер вопроса')
	n_teams_taken = models.SmallIntegerField(verbose_name='Число взявших команд')
	percent_teams_taken = models.SmallIntegerField(verbose_name='Процент взявших команд')
	percent_teams_taken_rounded = models.SmallIntegerField(verbose_name='Округлённый процент взявших команд')
	is_dropped = models.BooleanField(verbose_name='Снят ли по апелляции', db_index=True)
	def __str__(self):
		res = 'Вопрос № {} на турнире '.format(self.number) + str(self.tournament)
		if self.is_dropped:
			return res + '. Снят по апелляции'
		else:
			return res + '. Взяли {} команд, или {}%'.format(self.n_teams_taken, self.percent_teams_taken)
	class Meta:
		unique_together = (("tournament", "number"),)

class Team_on_tournament(models.Model):
	tournament = models.ForeignKey(Tournament, verbose_name='Турнир', on_delete=models.CASCADE)
	team = models.ForeignKey(Team, verbose_name='Команда', on_delete=models.CASCADE)
	current_name = models.CharField(verbose_name='Название на турнире', max_length=TEXT_FIELD_LENGTH, blank=True)
	base_name = models.CharField(verbose_name='Обычное название', max_length=TEXT_FIELD_LENGTH, blank=True)
	position = models.DecimalField(verbose_name='Занятое место', max_digits=6, decimal_places=1, default=None, null=True, blank=True, db_index=True)
	n_questions_taken = models.SmallIntegerField(verbose_name='Взято вопросов', default=None, null=True, blank=True, db_index=True)

	venue = models.ForeignKey(Venue, verbose_name='Площадка', on_delete=models.SET_NULL, default=None, null=True, blank=True)
	synch_request_id = models.IntegerField(verbose_name='ID заявки площадки', default=None, null=True, blank=True)

	bonus_a = models.SmallIntegerField(verbose_name='Бонус А', default=None, null=True, blank=True, db_index=True)
	tech_rating_rg = models.SmallIntegerField(verbose_name='Технический рейтинг', default=None, null=True, blank=True, db_index=True)
	tech_rating_rt = models.SmallIntegerField(verbose_name='Рейтинг фактического состава', default=None, null=True, blank=True, db_index=True)
	tech_rating_rb = models.SmallIntegerField(verbose_name='Рейтинг базового состава', default=None, null=True, blank=True, db_index=True)
	rating_r = models.SmallIntegerField(verbose_name='Релизный рейтинг', default=None, null=True, blank=True, db_index=True)
	predicted_position = models.DecimalField(verbose_name='Предсказанное место', max_digits=6, decimal_places=1,
		default=None, null=True, blank=True, db_index=True)
	bonus_b = models.SmallIntegerField(verbose_name='Бонус в рейтинге Б', default=None, null=True, blank=True, db_index=True)
	diff_bonus = models.SmallIntegerField(verbose_name='Прибавка к рейтингу', default=None, null=True, blank=True, db_index=True)
	is_included_in_rating = models.BooleanField(verbose_name='Учтен ли результат в рейтинге', db_index=True)
	def print_predicted_position(self):
		return comparing_util.print_semiwhole_number(self.predicted_position)
	def print_position(self):
		return comparing_util.print_semiwhole_number(self.position)
	def resultsAsArray(self):
		res = []
		for qt in self.questions_taken_set.order_by('block_number'):
			res += qt.asArray()
		return res
	def print_diff_bonus_with_brackets(self):
		diff_bonus = self.diff_bonus if self.diff_bonus else ''
		if self.is_included_in_rating:
			return diff_bonus
		return '<i>[{}]</i>'.format(diff_bonus)
	def get_players_list_asc(self):
		return self.player_on_tournament_set.select_related('player').order_by('player__surname', 'player__name', 'player__patronymic')
	def pr(self):
		print_obj_fields(self)
	def __str__(self):
		return 'Команда {} на турнире {}'.format(self.team, self.tournament)
	class Meta:
		unique_together = (("tournament", "team"),)

class Player_on_tournament(models.Model):
	team_on_tournament = models.ForeignKey(Team_on_tournament, verbose_name='Команда на турнире', on_delete=models.CASCADE)
	player = models.ForeignKey(Player, verbose_name='Игрок', on_delete=models.CASCADE)
	is_in_base_team = models.BooleanField(verbose_name='Входит ли в базовый состав')
	def update_if_needed(self, t_json, comment):
		return update_obj_if_needed(self, t_json, comment)
	def pr(self):
		print_obj_fields(self)
	def __str__(self):
		return f'Игрок {self.player} на турнире {self.team_on_tournament.tournament} за команду {self.team_on_tournament.team}'
	class Meta:
		unique_together = (("team_on_tournament", "player"),)

N_QUESTIONS_IN_BLOCK = 60
class Questions_taken(models.Model):
	team_on_tournament = models.ForeignKey(Team_on_tournament, verbose_name='Команда на турнире', on_delete=models.CASCADE)
	bits = models.BigIntegerField(verbose_name='Блок из 60 битов')
	block_number = models.SmallIntegerField(verbose_name='Номер блока, начиная с нуля', db_index=True)
	n_questions = models.SmallIntegerField(verbose_name='Число значащих битов')
	def asArray(self):
		res = [0] * self.n_questions
		cur_bits = self.bits
		for i in range(self.n_questions - 1, -1, -1):
			res[i] = cur_bits % 2
			cur_bits >>= 1
		return res
	def asString(self, question_dropped=None):
		res = ''
		cur_bits = self.bits
		for i in range(self.n_questions - 1, -1, -1):
			if (question_dropped is not None) and question_dropped[i]:
				symbol = 'X'
			else:
				symbol = str(cur_bits % 2)
			res = symbol + res
			cur_bits >>= 1
		return res
	def __str__(self):
		return '{}, вопросы №№ {}-{}: {}'.format(
			self.team_on_tournament,
			self.block_number * N_QUESTIONS_IN_BLOCK + 1,
			self.block_number * N_QUESTIONS_IN_BLOCK + self.n_questions,
			self.asString(),
		)
	class Meta:
		unique_together = (("team_on_tournament", "block_number"),)

class Statistics(models.Model):
	name = models.CharField(verbose_name='Название поля', max_length=30, db_index=True)
	value = models.IntegerField(verbose_name='Значение', default=0)
	date_added = models.DateField(verbose_name='Дата расчёта', auto_now_add=True)
	last_update = models.DateTimeField(verbose_name='Дата последнего обновления', auto_now=True)
	class Meta:
		unique_together = (("name", "date_added"),)
	def __str__(self):
		return f'{self.name} = {self.value} ({self.last_update.date})'

class Request_from_user(models.Model):
	tournament = models.ForeignKey(Tournament, verbose_name='Турнир', on_delete=models.CASCADE)
	team_list = models.CharField(verbose_name='Список команд', max_length=240, blank=True)
	request_ip = models.CharField(verbose_name='IP пользователя', max_length=20, blank=True)
	added_time = models.DateTimeField(verbose_name='Время запроса', auto_now_add=True, db_index=True)
	class Meta:
		index_together = [["tournament", "team_list"],]

class Request_quantity(models.Model):
	tournament = models.ForeignKey(Tournament, verbose_name='Турнир', on_delete=models.CASCADE)
	team_list = models.CharField(verbose_name='Список команд', max_length=240, blank=True)
	quantity = models.IntegerField(verbose_name='Число запросов', db_index=True)
	class Meta:
		unique_together = (("tournament", "team_list"),)

RATING_TYPES = (
	(0, 'Рейтинг Б'),
	(1, 'Отрезаем по одному лучшему и худшему результату'),
	(2, 'Отрезаем по [N/8] лучших и худших результатов'),
	(3, 'Отрезаем ⌈N/4⌉ худших результатов, очным турнирам даём вес 2'),
)
TOP_NUMBERS = [10, 100, 1000, 2000]
MIN_TOURNAMENTS_FOR_RATINGS = 4
class Team_rating(models.Model):
	team = models.ForeignKey(Team, verbose_name='Команда', on_delete=models.CASCADE)
	rating_type = models.SmallIntegerField(verbose_name='Способ расчёта', choices=RATING_TYPES, db_index=True)
	place = models.SmallIntegerField(verbose_name='Место', default=None, null=True)
	n_games = models.SmallIntegerField(verbose_name='Число игр в нужном промежутке', default=None, null=True)
	value = models.DecimalField(verbose_name='Рейтинг', max_digits=6, decimal_places=1)
	def pr(self):
		print_obj_fields(self)
	def __str__(self):
		return 'Команда {} с типом рейтинга {}: {}'.format(self.team, self.rating_type, self.value)
	class Meta:
		unique_together = (("team", "rating_type"),)
		index_together = [["team", "rating_type", "value"], ["team", "rating_type", "place"], ]

today = datetime.date.today()
season_start_year = today.year if (today.month >= 10) else (today.year - 1)
SEASON_START = datetime.date(season_start_year, 8, 31)
THREE_YEARS_BEFORE = today - datetime.timedelta(days=3 * 365 + 1)
LONG_AGO = datetime.date(1980, 1, 1)
TIME_SEGMENTS = (
	(0, 'В этом сезоне (с {})'.format(date2str(SEASON_START)), SEASON_START),
	(1, 'За последние три года (с {})'.format(date2str(THREE_YEARS_BEFORE)), THREE_YEARS_BEFORE),
	(2, 'За всегда', LONG_AGO),
)
TIME_SEGMENTS_FOR_TABLE_DEFS = ((x, y) for x, y, z in TIME_SEGMENTS)
class Team_most_often_partner(models.Model):
	team = models.ForeignKey(Team, verbose_name='Команда', on_delete=models.CASCADE)
	team2 = models.ForeignKey(Team, verbose_name='Вторая команда', related_name='team_most_often_partner_set2', on_delete=models.CASCADE)
	time_segment = models.SmallIntegerField(verbose_name='Временной промежуток', choices=TIME_SEGMENTS_FOR_TABLE_DEFS)
	only_offline_games = models.SmallIntegerField(verbose_name='Только ли среди очных турниров')
	n_games = models.IntegerField(verbose_name='Число игр между командами')
	def pr(self):
		print_obj_fields(self)
	def __str__(self):
		return f'Команда {self.team} сыграла {self.n_games} игр с командой {self.team2}'
	class Meta:
		unique_together = (("team", "team2", "time_segment", "only_offline_games"),)
		index_together = [["team", "time_segment", "only_offline_games", "n_games"], ]

MAX_TOGETHER_GAMES_TO_CHECK = 10
MIN_GAMES_IN_YEAR_TO_CHECK = 3
CLOSEST_TEAMS_TO_STORE = 5
class Team_close_to_mine(models.Model):
	team = models.ForeignKey(Team, verbose_name='Команда', on_delete=models.CASCADE)
	team2 = models.ForeignKey(Team, verbose_name='Вторая команда', related_name='team_close_to_mine_set2', on_delete=models.CASCADE)
	games_both_played = models.IntegerField(verbose_name=f'Сколько команды сыграли вместе (не больше {MAX_TOGETHER_GAMES_TO_CHECK})')
	difference = models.DecimalField(verbose_name='На сколько очков в среднем team брала больше', max_digits=5, decimal_places=2)
	abs_difference = models.DecimalField(verbose_name='abs(difference)', max_digits=5, decimal_places=2)
	deviation = models.DecimalField(verbose_name='Среднеквадратичное отклонение очков', max_digits=5, decimal_places=2)
	def pr(self):
		print_obj_fields(self)
	def __str__(self):
		return f'Команда {self.team} сыграла {self.games_both_played} игр с командой {self.team2} за последний год и в среднем брала на {self.difference} очков больше'
	class Meta:
		unique_together = (("team", "team2"),)
		index_together = [["team", "abs_difference", "deviation"], ]

class Player_in_team(models.Model):
	team = models.ForeignKey(Team, verbose_name='Команда', on_delete=models.CASCADE)
	player = models.ForeignKey(Player, verbose_name='Игрок', on_delete=models.CASCADE)
	time_segment = models.SmallIntegerField(verbose_name='Временной промежуток', choices=TIME_SEGMENTS_FOR_TABLE_DEFS)
	n_games = models.SmallIntegerField(verbose_name='Число игр за команду')
	def pr(self):
		print_obj_fields(self)
	def __str__(self):
		return 'Игрок {} сыграл за команду {} {} игр {}'.format(self.player, self.team, self.n_games, self.get_time_segment_display())
	class Meta:
		unique_together = (("team", "player", "time_segment"),)
		index_together = [["team", "player", "time_segment", "n_games"], ]

class Player_partner(models.Model):
	player = models.ForeignKey(Player, verbose_name='Игрок', on_delete=models.CASCADE)
	player2 = models.ForeignKey(Player, verbose_name='Второй игрок', related_name='player_partner_set2', on_delete=models.CASCADE)
	time_segment = models.SmallIntegerField(verbose_name='Временной промежуток', choices=TIME_SEGMENTS_FOR_TABLE_DEFS)
	only_offline_games = models.SmallIntegerField(verbose_name='Только ли среди очных турниров')
	n_games = models.IntegerField(verbose_name='Число игр с обоими за столом')
	def pr(self):
		print_obj_fields(self)
	def __str__(self):
		return 'Игрок {} сыграл {} игр за одним столом с игроком {}'.format(self.player, self.n_games, self.player2)
	class Meta:
		unique_together = (("player", "player2", "time_segment", "only_offline_games"),)
		index_together = [["player", "time_segment", "only_offline_games", "n_games"], ]

class Parameter(models.Model):
	name = models.CharField(verbose_name='Параметр', max_length=20, db_index=True, unique=True)
	value = models.CharField(verbose_name='Значение', max_length=100)

team_geos = {} # [''] * (models.Team.objects.all().aggregate(Max('id'))['id__max'] + 1)
def fill_team_geos():
	global team_geos
	team_geos = {}
	for tup in Team.objects.all().values_list('id', 'city_id', 'city__name', 'city__region_id', 'city__region__name',
			'city__region__district_id', 'city__region__district__name', 'city__country_id', 'city__country__name'):
		team_geos[tup[0]] = {'city_id': tup[1], 'city': tup[2], 'region_id': tup[3], 'region': tup[4],
			'district_id': tup[5], 'district': tup[6], 'country_id': tup[7], 'country': tup[8]}
# fill_team_geos()

def get_correct_mask(tournament, mask): # Hacks for API bugs
	if (tournament.id == 4375) and (len(mask) == tournament.questions_total - 1): # Hack for Monosov
		return mask + '0'
	if (tournament.id == 5074) and (len(mask) == tournament.questions_total + 1): # One more hack for Monosov
		return mask[:28] + mask[29:]
	return mask
