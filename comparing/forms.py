# -*- coding: utf-8 -*-
from django.contrib import messages
from django import forms

import datetime

from comparing import models

textarea = forms.Textarea(attrs={'rows':3, 'cols':50})

class QuestionTextForm(forms.ModelForm):
	class Meta:
		model = models.Question_text
		fields = ['tournament', 'number', 'lj_link', 'text', 'answer', 'criteria', 'comment', 'source', 'author', ]
		widgets = {
			'tournament': forms.HiddenInput(),
			'number': forms.HiddenInput(),
			'text': forms.Textarea(attrs={'rows':3, 'cols':50}),
			'answer': forms.Textarea(attrs={'rows':3, 'cols':50}),
			'criteria': forms.Textarea(attrs={'rows':3, 'cols':50}),
			'author': forms.Textarea(attrs={'rows':3, 'cols':50}),
			'comment': forms.Textarea(attrs={'rows':3, 'cols':50}),
			'source': forms.Textarea(attrs={'rows':3, 'cols':50}),
		}

class TournamentForm(forms.ModelForm):
	class Meta:
		model = models.Tournament
		fields = ['lj_link', 'kand_link', ]
		widgets = {
		}
